\input{../../latex-include/preamble.tex}
\input{../../latex-include/lstsetup.tex}
\input{../../latex-include/chemicalMacros.tex}


\newcommand{\dat}{12\textsuperscript{th} of December, 2019}
\newcommand{\tit}{Distillation in a Bubble Cap Distillation Column\\of a 2-propanol and  2-butanol mixture}


\title{\tit}
\author{
    \begin{tabular}{rll}
        Michael Bregy & \textbf{D}-CHAB  & \href{mailto:bregym@student.ethz.ch}{bregym@student.ethz.ch}\\ 
        Alexander Schoch & \textbf{D}-CHAB  & \href{mailto:schochal@student.ethz.ch}{schochal@student.ethz.ch}
    \end{tabular}
}
\fancyhead[L]{M. Bregy, A. Schoch}
\fancyhead[R]{Distillation}
\date{\dat}

\begin{document}
  \thispagestyle{plain}
 \maketitle
  \vspace{1cm}	
	\begin{figure}[H]
		\centering
		\Large
		\begin{tabular}{rl}
      Assistant: & Bodak Brigitta
		\end{tabular}
	\end{figure}
	\Large
	\begin{center}
		Laboratory for Mechanical Engineering at ETH Zürich
	\end{center}
	\normalsize
	\noindent\rule{\textwidth}{1pt}
  \vspace{-1cm}

	\paragraph{Abstract} 
The aim of the experiment was to determine the behavior of a 60 \% 2-propanol and 40 \% 2-butanol mixture with different reflux ratios ($R_{1}=2.67$, $R_{2}=5.00$) during a distillation process. Using Raoult's law equation for ideal case and the van-Laar equation for the real case, the vapor/liquid equilibrium (VLE) for both cases can be determined. One can see that the mixture had almost ideal behavior. In a next step the efficiency of the column was investigated by comparing the real number of stages ($n = 15$) to the theoritical stages ($n_{R_1} = 8$, $n_{R_2} = 11$), which was determined graphically with a McCabe-Thiele diagram. 

\noindent\rule{\textwidth}{1pt}	

	%Signature
	\vspace{\fill}
  \doubleSignature{Michael Bregy}{Alexander Schoch}{Zürich, }
	
\newpage
\section{Introduction}
Distillation is the process of heating a liquid or liquid-vapor mixture, to derive off  the vapor and collecting and condensing this vapor. Distillation is one of the oldest and most common method for chemical separation. 
The purpose of the following experiment is to remove a lighter component from a mixture of heavy components or the other way around. In this experiment, 2-propanol as the lighter one will be remove from a 60 \% 2-propanol and 40 \% 2-butanol mixture by different reflux ratios ($R_{1}=2.67$, $R_{2}=5.00$). By measuring the refractive index $n$ and the temperature at every stage of the column, it can be determined if the mixture reacts with real or ideal behavior, the corresponding T-xy diagram, weigth fraction of 2-propamol and temperature versus stage number of the column ($n = 15$) and at least, the McCabe-Thiele diagram for ideal and real case to determine the separation efficiency of the column.\cite{dstill} 

\section{Theory}
As from every stage of the column was withdrawn a sample, the refractive index $n$ can be measured and correlated to the mass fraction of 2-propanol $x_\text{prop}$ as following

\begin{equation}
  x_\text{prop} = -\frac{n-1.397}{0.0002004}.
\label{xprop}
\end{equation}

By exploiting the vapor/liquid equilibrium several components can be separated. To do this, vapor from the lower part of the column was brought into contact with liquid from the higher part of the column. Then, the vapor is enriched in the more volatile component and if the vapor and liquid are in counter-current and several equilibrium stages exist, one talks from rectification. Rectification is found by mass transfer between the two phases in equilibrium. If the components form a ideal mixture in both phases, the vapor/liquid equilibrium can be described by Raoult's law (eq.\ref{raoult}) as followed

\begin{equation}
  y_i*p = x_i*{{P_i}^\text{S}}(T),
\label{raoult}
\end{equation}
where $y_i$ is mole fraction of component i in vapor phase, $x_i$ is the mole fraction of component i in the liquid phase, $p$ the total presssure and ${{P_i}^S}(T)$ the vapor pressure of component i, which is temperature-dependent. For the vapor pressure of component i  an empirical correlation as the Antoine equation (eq. \ref{antoine}) can be used. 

\begin{equation}
  \log_{10}({{P_i}^\text{S}}(T)) = A_i - \frac{B_i}{T + C_i},
\label{antoine}
\end{equation}
where temperature T is given in [\SI{}{\celsius}] and the vapor pressure ${{P_i}^S}(T)$ in [mmHg] and $A_i$, $B_i$ and $C_i$ are constants which can find in appendix of \cite{dstill}. To correct the non-idealities in the liquid phase, one can introduce the activity coefficent $\gamma_i$ as described in equation \ref{acti}.

\begin{equation}
y_i*p = x_i*{\gamma_i}{P_i}(T),
\label{acti}
\end{equation}

As empirical correlation for the activity coefficient the van-Laar equation in a binary mixture (equation \ref{laar}) can be used.

\begin{equation}
\ln(\gamma_i) = A_{12}\left(\frac{A_{21}*x_2}{A_{12}*x_1 + A_{21}*x_2}\right)^2
\label{laar}
\end{equation}

where $A_{12}$ and $A_{21}$ are constants which can be found in the appendix of \cite{dstill}. 
\parmedskip
\parmedskip
In addition an overall (\ref{overall}) and component (\ref{component}) mass balance over the column can be formulated, where $x_i$ refers to the lighter component of the binary mixture (2-propanol). F, D and B refer to the flow rates in the feed, distillate and the bottoms. 

\begin{equation}
F = D + B
\label{overall}
\end{equation}
\begin{equation}
F{z_F} = D{x_D} + B{x_B}
\label{component}
\end{equation}

  
 \begin{figure}[H]
		\centering
		\includegraphics[width=6cm]{../plots/Column_introduction.pdf}
		\caption{Schematic representation of a distillation column \cite{dstill}. }
		\label{fig:column}
	\end{figure}
\par\bigskip


If a constant molar overflow (CMO) is assumed, the balances can be simplified. This assumption states that every mole of liquid mixture that evaporates, one mole of vapor condense. It is also assumed that both components has the same heat of vaporisation, which is only reasonable for similar components. The CMO assume the flow rates of the liquid and vapor to be constant within one column section (rectifying: $V$ and $L$ const., stripping: $V'$ and $L'$ const.). By means of figure \ref{fig:column} following mass balances for the rectifying (equation \ref{recti}) and stripping (equation \ref{strip}) part can be definded for component 1 (2-propanol).

\begin{equation}
{V_1}{y_1} = {L_0}{x_D}+{D}{x_D}
\label{recti}
\end{equation}
where the flow rate of the distillate $D$  defined in equation \ref{D_def} and $D$ is refeeded to the column.

\begin{equation}
D = \frac{L_0}{R}
\label{D_def}
\end{equation}
 
\begin{equation}
{V'_1}{y_1} = {L'_0}{x_B}+{B}{x_B}
\label{strip}
\end{equation}
\parmedskip
\parmedskip

The losing heat during the distillation can be calculated with an heat balance (equation \ref{heat_balance}), which is deviated from the mass balance. It includes the added heat in the reboiler ($Q_R$) and the removed heat in the condenser ($Q_C$). The heat of the feed $Q_F$ can be neglected. 

\begin{equation}
{Q_\text{lost}} = F*{h_F} + {Q_R} + {Q_C} - B*{h_B} - D*{h_D}
\label{heat_balance}
\end{equation}

The $h_i$ stands for the enthalpy of the mixture and can be expressed in his separate components. 
\begin{equation}
h_i = {h_1}{x_1} + {h_2}{x_2}
\label{enthalpy}
\end{equation}
Based on the temperature dependence of the enthalpy of the pure component, it can be calculated as
\begin{equation}
  {h_1} = {h_0} + {c_{\text{p},i}}\Delta T
\label{enthalpy_corr},
\end{equation}
where ${h_0}$ is the standard enthalpy, $\Delta T$ the temperature difference between the initial and final value of the temperature and under the assumption that the heat capacity $c_{p,i}$ of component i is time-independent.
\parmedskip
The removed heat $Q_C$ by the condenser can be determined as
\begin{equation}
  {Q_c} = F_{cooling}*{c_{\text{p,\ce{H2O}}}}*\Delta {T_\text{\ce{H2O}}},
\label{Q_c}
\end{equation}
whereas $F_{cooling}$ is the flow rate of the cooling water, ${c_{p,H2O}}$ the heat capacity of water and $\Delta {T_H20}$ is the temperature difference of the cooling water enters and exits the column.
\parmedskip
\parmedskip
\parmedskip
Under the CMO assumption, the working lines for both sections (rectifying equation \ref{cabe_recti}, stripping equation \ref{cabe_strip}) of the column can be calculated as followed.

\begin{equation}
y = \frac{L}{V}*x + \left(1-\frac{L}{V}*{x_D}\right)
\label{cabe_recti}
\end{equation} 

\begin{equation}
y = \frac{L'}{V'}*x + \left(1-\frac{L'}{V'}*{x_B}\right)
\label{cabe_strip}
\end{equation} 

An useful quantity is also the feed quantity $q$, which is defined in eq. \ref{q}. In our case the $q$ is set to 1, means that the feed is added as a boiling liquid. 

\begin{equation}
\frac{V-V'}{F} = q-1
\label{q}
\end{equation}

Knowing $x_D$ and $x_B$ and the feed compostion, drawing the working lines, one can construct the McCabe-Thiele diagram. This gives an information about the theoretical number of stages in the distillation column and can be compared to the real number of stages and a statement about the stage efficiency can be made \cite{dstill}.


\section{Experimental}
The distillation was executed in a bubble cap distillation column with 15 stages. 10 stages were part of the stripping section and 5 stages of the rectfying part. The 2- propanol and 2- butanol mixture (boiling liquid) was preheated by means of the reboiler and was then fed into the column between the 10th and 11th stage of it. The distillation column operated at different reflux ratio $R$ ($R_{1}=2.67$, $R_{2}=5.00$), whereby it is controlled by a electromagnet which switched between an on and off position. 
\parmedskip
A distillation process of a mixture of 2- propanol (M=\SI{60.1}{\gram\per\mole}, boiling point \SI{82}{\celsius},\cite{propanol}) and 2- butanol (M=\SI{74.12}{\gram\per\mole}, boiling point \SI{99}{\celsius},\cite{butanol}) was already started to reach the temperature steady-state at the reflux ratio of $R_{1}=2.67$. If this state was reached, the flow rates of the feed, distillate and bottom were measured. For this the valve of the pipe was closed and the time for a certain volume increase (\SI{10}{\milli\liter}) was measured to determine the flow rates.
\parmedskip
From the control element following parameters can be read: cooling water flow rate \SI{35}{\liter\per\hour}, feed preheater power \SI{160}{\watt} and temperature \SI{86.5}{\celsius}, temperature at the bottom \SI{87.5}{\celsius} and reboiler power \SI{595}{\watt}. Afterwards, from every of the 15 stages a sample was taken by a syringe through a bubble cap and the refractive index of the samples were measured by using a refractometer. 
\parmedskip
These procedures were repeated for a reflux ratio of $R_{2}=5.00$. It tooks again \SI{30}{\minute} until the steady state was reached. The cooling water flow rate is kept constant but the other parameters changed to: feed preheater power \SI{159}{\watt} and temperature \SI{87.3}{\celsius}, temperature at the bottom \SI{87.6}{\celsius} and reboiler power \SI{893}{\watt}.

\section{Results \& Discussion}
%TODO: evtl Resultate nachrechnen, unsicher ob korrekt
\subsection{Flow rates}
In tables \ref{flow_rate_1} and \ref{flow_rate_2}, the values of the measured and calculated flow rates of the feed, distillate and the bottom can be found. To calculate the flow rates it was assumed that the flow rate of the bottom was kept constant for both cases. In combination with equations \ref{overall} and \ref{component}, refractive index and equation \ref{xprop} and the densities of both components ($\rho_{prop}$ = \SI{0.78}{\gram\per\cubic\centi\meter} \cite{propanol}, $\rho_{but}$ = \SI{0.81}{\gram\per\cubic\centi\meter} \cite{butanol}), the flow rates can be determined and also the deviation from the measured flow rates.

\begin{mdframed}
  \begin{longtable}{rlll}
      \caption{Comparison of the measured and calculated flow rates of the feed, distillate and bottom at a reflux ratio of ${R_1}=2.67$. It was assumed that the flow rate of the bottom was equal in both cases.}
      \label{flow_rate_1} \\
      \toprule
     & \bfseries Measured / \si{\milli\mole\per\second} & \bfseries Calculated / \si{\milli\mole\per\second} & \bfseries Deviation / \si{\percent} \\
     \midrule\endhead
    {Feed} & 8.90 & 8.21 & 7.75 \\
    {Distillate} & 4.50 & 0.70 & 84.44 \\
    {Bottom}  & 7.51 & 7.51 & -\\ \bottomrule
  \end{longtable}
\end{mdframed}

\begin{mdframed}
  \begin{longtable}{rlll}
    \caption{Comparison of the measured and calculated flow rates of the feed, distillate and bottom at a reflux ratio of ${R_2}=5.00$.}
    \label{flow_rate_2} \\
    \toprule
    & \bfseries Measured / \si{\milli\mole\per\second} & \bfseries Calculated / \si{\milli\mole\per\second} & \bfseries Deviation / \si{\percent} \\ 
    \midrule\endhead
    {Feed} & 10.20 & 21.70 & 53.00  \\
    {Distillate} & 2.68 & 4.39 & 38.95  \\
    {Bottom} &  17.31 & 17.31 &-     \\  \bottomrule
  \end{longtable}
\end{mdframed}

\begin{mdframed}
  \begin{longtable}{rrlrlrl}
    \toprule
    & \multicolumn{2}{c}{\bfseries Measured / \si{\milli\mol\per\second}} & \multicolumn{2}{c}{\bfseries Calculated / \si{\milli\mol\per\second}} & \multicolumn{2}{c}{\bfseries Deviation / \si{\percent}} \\
    \midrule
    & $R = 2.67$ & $R = 5$ & $R = 2.67$ & $R = 5$ & $R = 2.67$ & $R = 5$ \\ \midrule
    Feed & 8.90 & 10.20 & 8.21 & 21.70 & 7.75 & 53.00 \\
    Distillate & 4.50 & 2.68 & 0.70 & 4.39 & 84.44 & 38.95 \\ 
    Bottom & 7.51 & 17.31 & 7.51 & 17.31 & 0 & 0 \\ \bottomrule
  \end{longtable}
\end{mdframed}

It can be seen that between the measured and calculated flow rates (except feed flow rate with ${R_1}=2.67$) are large deviations. Due to the fact, that the volumetric measured flow rates were determined by reading the volume by eye and stopping the time by hand, it is obviously that the measurements are not very exact.

\subsection{Heat balance}
%TODO: evtl Resultate nachrechnen, unsicher ob korrekt
From equations \ref{heat_balance} - \ref{Q_c}, the lost heat during the distillation process can be calculated. The standard enthalpies $h_0$ are \SI{-317.0}{\kilo\joule\per\mole} for 2- propanol \cite{stdenthalp_prop} and \SI{-342.7}{\kilo\joule\per\mole} for 2- butanol \cite{stdenthalp_but} and following values are received.

\begin{mdframed}
  \begin{longtable}{rlllll}
    \caption{Enthalpies of the feed, distillate and bottom and the heat removed by the condenser and added heat by the reboiler.}
    \label{enthalpies} \\
    \toprule
    $R$  & $H_\text{F}$ / \si{\kilo\joule\per\mol} & {$H_\text{D}$ / \si{\kilo\joule\per\mol}} & $H_\text{B}$ / \si{\kilo\joule\per\mol} & $Q_\text{C} (W)$ & $Q_\text{R} (W)$ \\ 
    \midrule\endhead
     2.67 & 327.05 & 317.44 & 327.82 & -488.02 & 528.49  \\
     5.00 & 327.31 & 317.69 & 329.75 & -488.02 & 501.64 \\ \bottomrule
  \end{longtable}
\end{mdframed}

So the heat loss during the column distillation was for the reflux ratio $R_1=2.67$ \SI{39.53}{\watt} and for $R_2=5.00$ \SI{13.58}{\watt}. The results were not very quantitative depends on the large deviations of the flow rates, means that the flow rates can vary strongly.


\subsection{Theoretical Part}

First, it is possible to gather quite some information about the mixture distilled in this experiment from literature data and some evaluation of that. \par\bigskip

Using Raoult's law (eq. \ref{raoult}), one can simulate the ideal equilibrium temperature for a given molar fraction $x_\text{prop}$. In oder to adapt this to a real mixture, the Antoine equation (eq. \ref{antoine}) can be used. From that, $x_\text{prop}$ and $y_\text{prop}$, respectively, can be plotted against the empirical temperature $\theta$. This comparison can be seen in fig. \ref{fig:equilibrium_temperature}.
%[ 1.31262012  1.29593189 -1.38507879] [0.00813698 0.00711375 0.01625588]


\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{minipage}{0.33\linewidth}
      \caption{This plot shows the equilibrium temperature for an ideal (solid line) and a real mixture (dashed line) for any given molar fraction $x_\text{prop}$.}
      \label{fig:equilibrium_temperature}
    \end{minipage}
    \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/x_T.pdf}
    \end{minipage}
  \end{figure}
\end{mdframed}

From the same data, the vapour-liquid-equilibrium plot can be created by just plotting $x_\text{prop}$ against $y_\text{prop}$ form before, as visible in fig. \ref{fig:VLE}.\par\smallskip

\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/VLE.pdf}
    \end{minipage}
    \begin{minipage}{0.33\linewidth}
      \caption{This plot shows the vapour-liquid-equilibrium for a binary propanol and butanol mixture. It is visible that propanol has a lower vapour pressure (which makes sense, as propanol is smaller and thus more volatile), as $y_\text{prop} \geq x_\text{prop}$, where $x_\text{prop}$ is the molar fraction of propanol in the liquid phase and $y_\text{prop}$ the one in the vapour phase.}
      \label{fig:VLE}
    \end{minipage}
  \end{figure}
\end{mdframed}

In fig. \ref{fig:VLE}, it is visible that the ideal and real curve are really, really close to each other. This means that it would have been possible to continue with the evaluation using $\gamma = 1$, but since we were using a programming language (\texttt{python} in this case), it was quite easy to just copy the function for the correction factor over to all other files, which is why the evaluation was done for a real mixture throughout.\par\smallskip

As visible in fig. \ref{fig:VLE}, it was tried to fit an exponential function onto the data for simplicity's sake. However, it turned out that this fit function would not quite work for the McCabe-Thiele plot for $R = 5$, as the fit function crosses the dashed line in fig. \ref{fig:mccabe-thiele_5}. Nonetheless, even though it was not used at all, the vapour-liquid-equilibrium can be expressed via 

  \begin{equation}
    y_\text{prop}(x_\text{prop}) = \num{1.313(8)} - \num{1.236(7)} \cdot e^{\num{-1.385(16)} x_\text{prop}}
  \end{equation}

  where the numbers in parentheses are standard deviations, not confidence intervals.


\subsection{Measured Part}

After looking at theoretical plots for a propanol-butanol mixture, we can go to the evaluation of our measured data.\par\bigskip

First, the refractive indices $n$ measured for each stage were converted into molar fractions and plotted against the stage they were taken from. Even though it is visible in fig. \ref{fig:x_stages} that higher stage numbers contain a higher concentration of propanol, this is not consistent at all. This is proabaly due to the very old refractometer, from which the data gathering is partially subjective (as there was a color gradient without clear border). Nonetheless, the overall trend coincides with the expectation that propanol is more volatile, thus has a higher vapour pressure $p_\text{prop}^\text{S}$ and ascends more in the distillation column, resulting in a higher molar fraction higher up.

\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{minipage}{0.33\linewidth}
      \caption{As propanol is more volatile than butanol, this compound will be found more concentrated up the distillation column. This is why a higher molar fraction $x_\text{prop}$ is visible for higher stages. Probably due to imprecise measurement, this data does not exactly behave the way it was expected.}
      \label{fig:x_stages}
    \end{minipage}
    \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/stages_x.pdf}
    \end{minipage}
  \end{figure}
\end{mdframed}

Similarly to behaviour of the propanol concentration in the distillation column, the temperature $\theta$ changes according to the amount of propanol in the mixture. As the boiling point of propanol is lower than the one of butanol, the temperature decreases with increasing height, as visible in fig. \ref{fig:temperature}.

\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/temperature.pdf}
    \end{minipage}
    \begin{minipage}{0.33\linewidth}
      \caption{This plot shows the temperature of each stage in the column. It is visible that higher stages (with a higher concentration of propanol) show lower temperatures, which is due to the lower boiling point of propanol. Also note the missing point at the 8\textsuperscript{th} stage. For all measurements, this value is lower than \SI{-100}{\celsius} and thus an outlier. This is probably due to a malfunctioning sensor.}
      \label{fig:temperature}
    \end{minipage}
  \end{figure}
\end{mdframed}

From the vapour-liquid-equilibrium diagram (fig. \ref{fig:VLE}) and the compositions of feed, distillate and bottom, it is now possible to theoretically derive the number of stages required for a separation. For that, the McCabe-Thiele diagram was created for both reflux ratios, as visible in fig. \ref{fig:mccabe-thiele}. \par\smallskip

From this diagram, one can count the number of \quotes{stairs}, which is $n_{R_1} = 8$ for $R_1 = 2.67$ and $n_{R_2} = 11$ for $R_2 = 5$. Therefore, our distillation column containing 15 stages contained more stages than necessary for our distillation.

\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\linewidth}
      \includegraphics[width=\linewidth]{../plots/mccabe_thiele.pdf}
      \caption{McCabe-Thiele diagram for $R = 2.67$}
      \label{fig:mccabe-thiele_2}
    \end{subfigure}
    \begin{subfigure}{0.49\linewidth}
      \includegraphics[width=\linewidth]{../plots/mccabe_thiele_2.pdf}
      \caption{McCabe-Thiele diagram for $R = 5$}
      \label{fig:mccabe-thiele_5}
    \end{subfigure}
    \caption{McCabe-Thiele plots for both reflux ratios. It is visible that more stages are necessary for $R_2 = 5$, even though both cases need less stages than our distillation column contained.}
    \label{fig:mccabe-thiele}
  \end{figure}
\end{mdframed}





\section{Conclusions}

Generally, the evaluation of the experiment worked quite well. However, the measurement of the refracitve index $n$ using an ancient refractometer resulted in quite unreliable data. This was not too much of a problem, as this data was not that crucial in this experiment.

\newpage
 
  
  \bibliographystyle{unsrt}
  \bibliography{Literature_distillation}
  
   \newpage

\section{Appendix} 
\subsection{Steady-State}
\begin{mdframed}
  \begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\linewidth}
      \includegraphics[width=\linewidth]{../plots/temperature_2.pdf}
      \caption{Temperature development for $R = 2.67$}
    \end{subfigure}
    \begin{subfigure}{0.49\linewidth}
      \includegraphics[width=\linewidth]{../plots/temperature_5.pdf}
      \caption{Temperature development for $R = 5$}
    \end{subfigure}
    \caption{This plot shows the temperature course for each stage, showing that the distillation column was running in steady-state.}
  \end{figure}
\end{mdframed}


\subsection{Code}
\includecode{../python/x_T.py}{\texttt{python} code for fig. \ref{fig:equilibrium_temperature}. Also, credit goes to Asbj\o rn Rasmussen and Daniel Vadakumcheril for helping us out with the evaluation.}{py}
\includecode{../python/VLE.py}{\texttt{python} code for fig. \ref{fig:VLE}. Also, credit goes to Asbj\o rn Rasmussen and Daniel Vadakumcheril for helping us out with the evaluation.}{py}
\includecode{../python/stages_x.py}{\texttt{python} code for fig. \ref{fig:x_stages}}{py}
\includecode{../python/temperature.py}{\texttt{python} code for fig. \ref{fig:temperature}}{py}
\includecode{../python/mccabe_thiele.py}{\texttt{python} code for fig. \ref{fig:mccabe-thiele_2}. Also, credit goes to Asbj\o rn Rasmussen and Daniel Vadakumcheril for helping us out with the evaluation.}{py}
\includecode{../python/mccabe_thiele_2.py}{\texttt{python} code for fig. \ref{fig:mccabe-thiele_5}. Also, credit goes to Asbj\o rn Rasmussen and Daniel Vadakumcheril for helping us out with the evaluation.}{py}
\includepdf[pages=-]{../include/labjournal.pdf}
\end{document}
