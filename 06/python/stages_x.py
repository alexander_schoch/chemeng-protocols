# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ureg = UnitRegistry()
Q_ = ureg.Quantity

def x(n): 
    wp = (n -1.397) /( -0.02004)
    wb = 1 - wp
    Mp = 60.1
    Mb = 74.122
    return ( wp / Mp ) / (( wp / Mp ) + ( wb / Mb ))


###############
# System vars #
###############

NUM_POINTS = 10000
TEMP = np.linspace(50,150, NUM_POINTS)

#############
# Constants #
#############

ptmp = 100000 * ureg.pascal # Pa
ptmp.ito('mmHg')
p = ptmp.magnitude # torr

########
# Data #
########

n_prop_2 = np.array([1.3861, 1.3832, 1.3842, 1.3840, 1.3860, 1.3855, 1.3849, 1.3843, 1.3841, 1.3841, 1.3832, 1.3810, 1.3795, 1.3785, 1.3780])
nF_prop_2 = np.array([1.3848, 1.3773, 1.3855]) # F D B
n_prop_5 = np.array([1.3871, 1.3852, 1.3851, 1.3832, 1.3831, 1.3840, 1.3838, 1.3825, 1.3821, 1.3820, 1.3812, 1.3795, 1.3792, 1.3781, 1.3775])
nF_prop_5 = np.array([1.3850, 1.3775, 1.3869])


B_2 = 0.01 * ureg.liter / ( 16 * ureg.second ) # L s-1
F_2 = 0.01 * ureg.liter / ( 13.6 * ureg.second ) # L s-1
D_2 = 0.01 * ureg.liter / ( 47.23 * ureg.second ) # L s-1
B_5 = 0.01 * ureg.liter / ( 14.55 * ureg.second ) # L s-1
F_5 = 0.01 * ureg.liter / ( 11.84 * ureg.second ) # L s-1
D_5 = 0.01 * ureg.liter / ( 49.95 * ureg.second ) # L s-1


# Cooling
C = 35 * ureg.liter / ( 3600 * ureg.second ) # L s-1

# Power
P_feed_2 = 160 * ureg.watt # W
P_bottom_2 = 595 * ureg.watt # W
P_feed_5 = 159 * ureg.watt # W
P_bottom_5 = 893 * ureg.watt # W




x_prop_2 = x(n_prop_2)
xF_prop_2 = x(nF_prop_2)
x_prop_5 = x(n_prop_5)
xF_prop_5 = x(nF_prop_5)



plt.scatter(np.arange(1,16), x_prop_2, label=r'$R = 2.67$', c='black', marker='o', s=10)
#plt.scatter(np.arange(1,16), x_prop_5, label=r'$R = 5$', c='black', s=10, facecolors='blue', marker='o')
plt.plot(np.arange(1,16), x_prop_5, 'o', label=r'$R = 5$', color='black', mfc='none', ms=4)


# Axis labels
plt.xlabel(r'stage', fontsize=16)
plt.ylabel(r'$x_\text{prop}$', fontsize=16)


# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/stages_x.pdf')
