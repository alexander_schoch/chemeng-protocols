# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from pint import UnitRegistry
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ureg = UnitRegistry()


#############
# Constants #
#############

#p = 100000 # Pa
p = 750.06168282 * ureg.torr # mmHg 


########
# Data #
########

n_prop_2 = np.array([1.3861, 1.3832, 1.3842, 1.3840, 1.3860, 1.3855, 1.3849, 1.3843, 1.3841, 1.3841, 1.3832, 1.3810, 1.3795, 1.3785, 1.3780])
n_prop_5 = np.array([1.3871, 1.3852, 1.3851, 1.3832, 1.3831, 1.3840, 1.3838, 1.3825, 1.3821, 1.3820, 1.3812, 1.3795, 1.3792, 1.3781, 1.3775])
x_prop_2 = (n_prop_2 - 1.397) / -0.02004
x_prop_5 = (n_prop_5 - 1.397) / -0.02004

B_2 = 0.01 / 16 # L s-1
F_2 = 0.01 / 13.6 # L s-1
D_2 = 0.01 / 47.23 # L s-1
B_5 = 0.01 / 14.55 # L s-1
F_5 = 0.01 / 11.84 # L s-1
D_5 = 0.01 / 49.95 # L s-1

# Cooling
C = 35 / 3600 # L s-1

# Power
P_feed_2 = 160 # W
P_bottom_2 = 595 # W
P_feed_5 = 159 # W
P_bottom_5 = 893 # W

#############
# Functions #
#############

def activition_coeff(x1, x2, is_ideal):
    A12 = 0.0730 
    A21 = 0.0785
    if (is_ideal):
        return 1
    else:
        return np.exp(A12 * ((A21 * x2) / (A12 * x1 + A21 * x2))**2) 

def vapour_pressure (T, is_prop):
    A_prop = 8.87829
    B_prop = 2010.330
    C_prop = 252.636
    A_but = 7.47429
    B_but = 1314.188
    C_but = 186.500
    if (is_prop):
        return 10**(A_prop - B_prop / (T + C_prop))
    else:
        return 10**(A_but - B_but / (T + C_but))

##############
# Evaluation #
##############

# TODO: insert temperatures
temperature = 80 # C
xarr = np.linspace(0,1,100)
yarr = np.array([i * activition_coeff(i, 1-i, False) * vapour_pressure(temperature, True) / p for i in xarr])
yarr2 = np.array([i * activition_coeff(i, 1-i, True) * vapour_pressure(temperature, True) / p for i in xarr])

# Axis labels
plt.xlabel(r'$x_\text{prop}$', fontsize=16)
plt.ylabel(r'$y_\text{prop}$', fontsize=16)

plt.plot(xarr, yarr, linewidth=1, color='black')
plt.plot(xarr, yarr2, linewidth=1, color='red')
#y_prop_2 = x_prop_2 * activition_coeff(x_prop_2, 1-x_prop_2, False) * vapour_pressure(temperature, True) / p


# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/Equilibrium.pdf')
