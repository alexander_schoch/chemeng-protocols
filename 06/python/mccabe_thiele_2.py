# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ureg = UnitRegistry()
Q_ = ureg.Quantity

###############
# System vars #
###############

NUM_POINTS = 100 
TEMP = np.linspace(50,150, NUM_POINTS)

#############
# Constants #
#############

ptmp = 100000 * ureg.pascal # Pa
ptmp.ito('mmHg')
p = ptmp.magnitude # torr

########
# Data #
########

n_prop_2 = np.array([1.3861, 1.3832, 1.3842, 1.3840, 1.3860, 1.3855, 1.3849, 1.3843, 1.3841, 1.3841, 1.3832, 1.3810, 1.3795, 1.3785, 1.3780])
nF_prop_2 = np.array([1.3848, 1.3773, 1.3855]) # F D B
n_prop_5 = np.array([1.3871, 1.3852, 1.3851, 1.3832, 1.3831, 1.3840, 1.3838, 1.3825, 1.3821, 1.3820, 1.3812, 1.3795, 1.3792, 1.3781, 1.3775])
nF_prop_5 = np.array([1.3850, 1.3775, 1.3869])


B_2 = 0.01 * ureg.liter / ( 16 * ureg.second ) # L s-1
F_2 = 0.01 * ureg.liter / ( 13.6 * ureg.second ) # L s-1
D_2 = 0.01 * ureg.liter / ( 47.23 * ureg.second ) # L s-1
B_5 = 0.01 * ureg.liter / ( 14.55 * ureg.second ) # L s-1
F_5 = 0.01 * ureg.liter / ( 11.84 * ureg.second ) # L s-1
D_5 = 0.01 * ureg.liter / ( 49.95 * ureg.second ) # L s-1


# Cooling
C = 35 * ureg.liter / ( 3600 * ureg.second ) # L s-1

# Power
P_feed_2 = 160 * ureg.watt # W
P_bottom_2 = 595 * ureg.watt # W
P_feed_5 = 159 * ureg.watt # W
P_bottom_5 = 893 * ureg.watt # W


#############
# Functions #
#############

def activition_coeff(x1, x2, is_ideal):
    A12 = 0.0730 
    A21 = 0.0785
    if (is_ideal):
        return 1
    else:
        return np.exp(A12 * ((A21 * x2) / (A12 * x1 + A21 * x2))**2) 

def vapour_pressure (T, is_prop):
    A_prop = 8.87829
    B_prop = 2010.330
    C_prop = 252.636
    A_but = 7.47429
    B_but = 1314.188
    C_but = 186.500
    if (is_prop):
        return 10**(A_prop - B_prop / (T + C_prop)) #* ureg.mmHg
    else:
        return 10**(A_but - B_but / (T + C_but)) #* ureg.mmHg

def x(n): 
    wp = (n -1.397) /( -0.02004)
    wb = 1 - wp
    Mp = 60.1
    Mb = 74.122
    return ( wp / Mp ) / (( wp / Mp ) + ( wb / Mb ))

# deinfe the stripping section
def linear (x ,m , b):
    return x * m + b

# define a function for the rectifying section
def rectsec (x, R, xd):
    return x *(1/(1+1/ R ) ) + xd *(1 -(1/(1+1/ R ) ) )

# find a function for the x - y curve ( will be usefull in the mcCabe - Thiele Plot )
def xy_curve (x, A, B, C):
    return A - B * np . exp ( C * x )

def y(x_prop):
    x_but = 1 - x_prop
    pS_prop = vapour_pressure ( TEMP, True )
    pS_but  = vapour_pressure ( TEMP, False )
    pressure_prop = activition_coeff(x_prop, x_but, False) * x_prop * pS_prop 
    pressure_but = activition_coeff(x_but, x_prop, False) * x_but * pS_but 
    ptot = pressure_prop + pressure_but
    idx = np.amax(np.where(ptot < p )) 
    thetac = TEMP [ idx ] 
    p1c = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
    p2c = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
    ptotc = ptot [ idx ] # Gesamtdruck ( mbar )
    return pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase




################
# calculations #
################

x_prop_2 = x(n_prop_2)
xF_prop_2 = x(nF_prop_2)
x_prop_5 = x(n_prop_5)
xF_prop_5 = x(nF_prop_5)


x_prop = np.linspace(0,1,NUM_POINTS)
x_but = 1 - x_prop

# Initialisation
thetac = np.linspace (0 ,0 , NUM_POINTS )
ptotc = np.linspace (0 ,0 , NUM_POINTS )
p2c = np.linspace (0 ,0 , NUM_POINTS )
p1c = np.linspace (0 ,0 , NUM_POINTS )
y2c = np.linspace (0 ,0 , NUM_POINTS )
y1c = np.linspace (0 ,0 , NUM_POINTS )


# find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
#for i in np.arange(0,NUM_POINTS):
#    pS_prop = vapour_pressure ( TEMP, True )
#    pS_but  = vapour_pressure ( TEMP, False )
#    pressure_prop = activition_coeff(x_prop[i], x_but[i], True) * x_prop[i] * pS_prop 
#    pressure_but = activition_coeff(x_but[i], x_prop[i], True) * x_but[i] * pS_but 
#    ptot = pressure_prop + pressure_but
#    idx = np.amax(np.where(ptot < p )) 
#    thetac [ i ] = TEMP [ idx ] 
#    p1c[i] = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
#    p2c [ i ] = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
#    ptotc [ i ] = ptot [ idx ] # Gesamtdruck ( mbar )
#    y1c [ i ] = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
#    y2c [ i ] = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase

#plt.plot(x_prop, y1c, linewidth=1, color='black', label='ideal')

# find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
for i in np.arange(0,NUM_POINTS):
    pS_prop = vapour_pressure ( TEMP, True )
    pS_but  = vapour_pressure ( TEMP, False )
    pressure_prop = activition_coeff(x_prop[i], x_but[i], False) * x_prop[i] * pS_prop 
    pressure_but = activition_coeff(x_but[i], x_prop[i], False) * x_but[i] * pS_but 
    ptot = pressure_prop + pressure_but
    idx = np.amax(np.where(ptot < p )) 
    thetac [ i ] = TEMP [ idx ] 
    p1c[i] = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
    p2c [ i ] = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
    ptotc [ i ] = ptot [ idx ] # Gesamtdruck ( mbar )
    y1c [ i ] = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
    y2c [ i ] = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase

#plt.plot(x_prop, y1c, linewidth=1, color='black', linestyle='-')
plt.plot(x_prop, x_prop, linewidth=1, color='black', linestyle='-')



#################
# McCabe-Thiele #
#################
parms , covariance = curve_fit ( xy_curve , x_prop , y1c )
#plt.plot(x_prop, xy_curve(x_prop, *parms), linestyle=':', linewidth=1, color='black')
y_prop = np.array([y(i) for i in x_prop])
plt.plot(x_prop, y_prop, linestyle='-', linewidth=1, color='black')
x_strip = ( xF_prop_2[2], xF_prop_2[0] )
y_strip = ( xF_prop_2[2], rectsec (xF_prop_2[0], 2.67, xF_prop_2[1] ))
parms_strip , covariance_strip = curve_fit (linear , x_strip , y_strip)

xarr = np . linspace (0 ,1 ,100)
# ###############
# crosssection #
# ###############


x = 0
while (linear(x,  *parms_strip) <= rectsec(x, 2.67, xF_prop_2[1])):
    xcross = x
    x += 0.0001

xstairs = xF_prop_2[1]
vertices_x = np.array ( xstairs )
vertices_y = np.array ( rectsec(xstairs, 2.67, xF_prop_2[1] ))

tobe = True

while ( xstairs >= xF_prop_2[2] ):
    if ( linear(xstairs, *parms_strip) > rectsec(xstairs, 2.67, xF_prop_2[1] )):
        # we use pl2
        if ( tobe ) :
            # horizontal movement
            xtmp = xstairs
            ytmp = rectsec ( xstairs, 2.67, xF_prop_2[1] )
            while ( ytmp <= y(xtmp)):
                xcrs = xtmp
                xtmp -= 0.001
            vertices_x = np.append( vertices_x, xtmp )
            vertices_y = np.append( vertices_y, ytmp )
            xstairs = xtmp
        else :
            # vertical movement
            vertices_x = np.append ( vertices_x, xtmp )
            vertices_y = np.append ( vertices_y, rectsec ( xtmp, 2.67, xF_prop_2[1] ))
    else :
        # we use pl1
        if ( tobe ) :
            # horizontal movement
            xtmp = xstairs
            ytmp = linear ( xstairs , *parms_strip )
            while ( ytmp <= y(xtmp)):
                xcrs = xtmp
                xtmp -= 0.001
            vertices_x = np.append ( vertices_x, xtmp )
            vertices_y = np.append ( vertices_y, ytmp )
            xstairs = xtmp
        else :
            # vertical movement
            vertices_x = np.append ( vertices_x, xtmp )
            vertices_y = np.append ( vertices_y, linear ( xtmp , *parms_strip ))
    tobe = not tobe

for i in range (1, len ( vertices_x )):
    plt.plot([vertices_x[i-1], vertices_x[i]], [vertices_y[i-1], vertices_y[i]], color='black', linestyle='-', linewidth=0.5)
plt.plot ([vertices_x[-1], vertices_x[-1]], [vertices_y[-1], vertices_x[-1]],  color='black', linestyle='-', linewidth=0.5)

plt.scatter(xcross , linear (xcross, *parms_strip), c='black')

plt.plot([xF_prop_2[1], xF_prop_2[0]], [xF_prop_2[1], rectsec(xF_prop_2[0], 2.67, xF_prop_2[1])], color="black", linestyle='--', linewidth=1)
plt.plot ([xF_prop_2[2], xF_prop_2[0]], [xF_prop_2[2], linear(xF_prop_2[0], *parms_strip)], color="black", linestyle='--', linewidth=1)

# Axis labels
plt.xlabel(r'$x_\text{prop}$', fontsize=16)
plt.ylabel(r'$y_\text{prop}$', fontsize=16)

plt.xlim(0,1)
plt.ylim(0,1)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/mccabe_thiele_2.pdf')
