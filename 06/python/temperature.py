# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')


DATA_2 = '../data/data2.txt'
DATA_5 = '../data/data5.txt'
START_TIME =1500 

data2 = np.genfromtxt(DATA_5, skip_header=4).transpose()

T_mean = np.linspace(0,0,14)
index = 0
for i in np.arange(1,16):
    if i == 8:
        print('outlier:', np.mean(data2[i][START_TIME:-1]))
        continue
    #plt.plot(data2[0][START_TIME:-1], data2[i][START_TIME:-1], linewidth=1)
    T_mean[index] = np.mean(data2[i][START_TIME:-1])
    index += 1


stages = [1,2,3,4,5,6,7,9,10,11,12,13,14,15]

plt.scatter(stages, T_mean, c='black', s=10)


# Axis labels
plt.xlabel(r'stage', fontsize=16)
plt.ylabel(r'$\theta$ / \si{\celsius}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/temperature.pdf')
