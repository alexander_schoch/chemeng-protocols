# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ureg = UnitRegistry()
Q_ = ureg.Quantity

###############
# System vars #
###############

NUM_POINTS = 10000
TEMP = np.linspace(50,150, NUM_POINTS)

#############
# Constants #
#############

ptmp = 100000 * ureg.pascal # Pa
ptmp.ito('mmHg')
p = ptmp.magnitude # torr

#############
# Functions #
#############

def activition_coeff(x1, x2, is_ideal):
    A12 = 0.0730 
    A21 = 0.0785
    if (is_ideal):
        return 1
    else:
        return np.exp(A12 * ((A21 * x2) / (A12 * x1 + A21 * x2))**2) 

def vapour_pressure (T, is_prop):
    A_prop = 8.87829
    B_prop = 2010.330
    C_prop = 252.636
    A_but = 7.47429
    B_but = 1314.188
    C_but = 186.500
    if (is_prop):
        return 10**(A_prop - B_prop / (T + C_prop)) #* ureg.mmHg
    else:
        return 10**(A_but - B_but / (T + C_but)) #* ureg.mmHg


################
# calculations #
################

x_prop = np.linspace(0,1,NUM_POINTS)
x_but = 1 - x_prop

# Initialisation
thetac = np.linspace (0 ,0 , NUM_POINTS )
ptotc = np.linspace (0 ,0 , NUM_POINTS )
p2c = np.linspace (0 ,0 , NUM_POINTS )
p1c = np.linspace (0 ,0 , NUM_POINTS )
y2c = np.linspace (0 ,0 , NUM_POINTS )
y1c = np.linspace (0 ,0 , NUM_POINTS )


# find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
for i in np.arange(0,NUM_POINTS):
    pS_prop = vapour_pressure ( TEMP, True )
    pS_but  = vapour_pressure ( TEMP, False )
    pressure_prop = activition_coeff(x_prop[i], x_but[i], True) * x_prop[i] * pS_prop 
    pressure_but = activition_coeff(x_but[i], x_prop[i], True) * x_but[i] * pS_but 
    ptot = pressure_prop + pressure_but
    idx = np.amax(np.where(ptot < p )) 
    thetac [ i ] = TEMP [ idx ] 
    p1c[i] = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
    p2c [ i ] = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
    ptotc [ i ] = ptot [ idx ] # Gesamtdruck ( mbar )
    y1c [ i ] = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
    y2c [ i ] = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase

plt.plot(x_prop, thetac, linewidth=1, color='black', label='ideal')
plt.plot(y1c, thetac, linewidth=1, color='black')

# find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
for i in np.arange(0,NUM_POINTS):
    pS_prop = vapour_pressure ( TEMP, True )
    pS_but  = vapour_pressure ( TEMP, False )
    pressure_prop = activition_coeff(x_prop[i], x_but[i], False) * x_prop[i] * pS_prop 
    pressure_but = activition_coeff(x_but[i], x_prop[i], False) * x_but[i] * pS_but 
    ptot = pressure_prop + pressure_but
    idx = np.amax(np.where(ptot < p )) 
    thetac [ i ] = TEMP [ idx ] 
    p1c[i] = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
    p2c [ i ] = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
    ptotc [ i ] = ptot [ idx ] # Gesamtdruck ( mbar )
    y1c [ i ] = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
    y2c [ i ] = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase

plt.plot(x_prop, thetac, linewidth=1, color='black', linestyle=':', label='real')
plt.plot(y1c, thetac, linewidth=1, color='black', linestyle=':')


# Axis labels
plt.xlabel(r'$x_\text{prop}$', fontsize=16)
plt.ylabel(r'$\theta$ / \si{\celsius}', fontsize=16)

plt.xlim(0,1)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/x_T.pdf')
