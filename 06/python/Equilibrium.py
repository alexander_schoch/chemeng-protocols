# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from pint import UnitRegistry
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ureg = UnitRegistry()
Q_ = ureg.Quantity


#############
# Constants #
#############

p = 100000 * ureg.pascal # Pa
p.ito('mmHg')

########
# Data #
########

n_prop_2 = np.array([1.3861, 1.3832, 1.3842, 1.3840, 1.3860, 1.3855, 1.3849, 1.3843, 1.3841, 1.3841, 1.3832, 1.3810, 1.3795, 1.3785, 1.3780])
n_prop_5 = np.array([1.3871, 1.3852, 1.3851, 1.3832, 1.3831, 1.3840, 1.3838, 1.3825, 1.3821, 1.3820, 1.3812, 1.3795, 1.3792, 1.3781, 1.3775])
x_prop_2 = (n_prop_2 - 1.397) / -0.02004
x_prop_5 = (n_prop_5 - 1.397) / -0.02004

B_2 = 0.01 * ureg.liter / ( 16 * ureg.second ) # L s-1
F_2 = 0.01 * ureg.liter / ( 13.6 * ureg.second ) # L s-1
D_2 = 0.01 * ureg.liter / ( 47.23 * ureg.second ) # L s-1
B_5 = 0.01 * ureg.liter / ( 14.55 * ureg.second ) # L s-1
F_5 = 0.01 * ureg.liter / ( 11.84 * ureg.second ) # L s-1
D_5 = 0.01 * ureg.liter / ( 49.95 * ureg.second ) # L s-1


# Cooling
C = 35 * ureg.liter / ( 3600 * ureg.second ) # L s-1

# Power
P_feed_2 = 160 * ureg.watt # W
P_bottom_2 = 595 * ureg.watt # W
P_feed_5 = 159 * ureg.watt # W
P_bottom_5 = 893 * ureg.watt # W

#############
# Functions #
#############

def activition_coeff(x1, x2, is_ideal):
    A12 = 0.0730 
    A21 = 0.0785
    if (is_ideal):
        return 1
    else:
        return np.exp(A12 * ((A21 * x2) / (A12 * x1 + A21 * x2))**2) 

def vapour_pressure (T, is_prop):
    A_prop = 8.87829
    B_prop = 2010.330
    C_prop = 252.636
    A_but = 7.47429
    B_but = 1314.188
    C_but = 186.500
    if (is_prop):
        return 10**(A_prop - B_prop / (T + C_prop)) * ureg.mmHg
    else:
        return 10**(A_but - B_but / (T + C_but)) * ureg.mmHg

NUM_POINTS = 500

def T(x_prop, is_ideal):
    TEMP = np.linspace(50,150, NUM_POINTS)
    x_but = 1 - x_prop
    if (is_ideal):
        # find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
        pS_prop = vapour_pressure ( TEMP, True )
        pS_but  = vapour_pressure ( TEMP, False )
        pressure_prop = activition_coeff(x_prop, x_but, True) * x_prop * pS_prop 
        pressure_but = activition_coeff(x_but, x_prop, True) * x_but * pS_but 
        ptot = pressure_prop + pressure_but
        idx = np.amax(np.where(ptot < p )) 
        thetac = TEMP [ idx ] 
        p1c = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
        p2c = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
        ptotc = ptot [ idx ] # Gesamtdruck ( mbar )
        y1c = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
        y2c = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase
        return thetac 
    else:
        # find at what Temperature ptot = p1 + p2 ( credit: Erich Meister , ETHZ , 2010)
        pS_prop = vapour_pressure ( TEMP, True )
        pS_but  = vapour_pressure ( TEMP, False )
        pressure_prop = activition_coeff(x_prop, x_but, False) * x_prop * pS_prop 
        pressure_but = activition_coeff(x_but, x_prop, False) * x_but * pS_but 
        ptot = pressure_prop + pressure_but
        idx = np.amax(np.where(ptot < p )) 
        thetac = TEMP [ idx ] 
        p1c = pressure_prop[idx] # Partialdruck von Substanz 1 ( mbar )
        p2c = pressure_but [ idx ] # Partialdruck von Substanz 2 ( mbar )
        ptotc = ptot [ idx ] # Gesamtdruck ( mbar )
        y1c = pressure_prop [ idx ] / ptot [ idx ] # Molenbruch von 1 in der Gasphase
        y2c = pressure_but [ idx ] / ptot [ idx ] # Molenbruch von 2 in der Gasphase
        return thetac


##############
# Evaluation #
##############


# Axis labels
plt.xlabel(r'$x_\text{prop}$', fontsize=16)
plt.ylabel(r'$y_\text{prop}$', fontsize=16)

xarr = np.linspace(0,1,500)
yarr = xarr * activition_coeff(xarr, 1-xarr, True) * vapour_pressure(T(xarr, True), False) / p
plt.plot(xarr, yarr, linewidth=1, color='black', label=r'ideal')
yarr = xarr * activition_coeff(xarr, 1-xarr, False) * vapour_pressure(T(xarr, False), False) / p
plt.plot(xarr, yarr, linewidth=1, color='black', label=r'ideal', linestyle=':')
#y_prop_2 = x_prop_2 * activition_coeff(x_prop_2, 1-x_prop_2, False) * vapour_pressure(temperature, True) / p


# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/Equilibrium.pdf')
