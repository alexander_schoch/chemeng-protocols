# Chemical Engineering Protocols

This repository contains all chemical engineering praktikum reports of [Michael Bregy](mailto:bregym@student.ethz.ch) and [Alexander Schoch](schochal@student.ethz.ch).

## Navigation

- The final reports can be found at `[experiment number]/src/[name].pdf`
- all `python` code for evaluation and plots is located in either `[experiment number]/python/` or `[experiment number]/py/`
- all `matlab` code for evaluation and plots is locaded in `[experiment number]/matlab/`
- all data is located in `[experiment number]/data/`

## Our Experiments

- 01: sequential programming (labVIEW)
- 02: Diffusion of CO2, Heat Transfer, Light triggered dissolution of gold
- 03: Catalysis
- 04: Enzyme Kinetics
- 05: Parametric Sensitivity
- 06: Distillation

## License

All reports have been written by Michael Bregy and Alexander Schoch and are licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
