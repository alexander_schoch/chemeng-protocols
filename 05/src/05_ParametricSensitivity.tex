% This file is a template for the protocols


\input{../../latex-include/preamble.tex}
\input{../../latex-include/lstsetup.tex}
\input{../../latex-include/chemicalMacros.tex}


\usepackage{etoolbox}

\newcommand{\dat}{5\textsuperscript{st} of December, 2019}
\newcommand{\tit}{Parametric Sensitivity investigated on Hydrolysis of Acetic Anhydride in a Batch Reactor }
\newcommand{\dd}{\text{d}}

\title{\tit}
\author{
    \begin{tabular}{rll}
        Michael Bregy & \textbf{D}-CHAB  & \href{mailto:bregym@student.ethz.ch}{bregym@student.ethz.ch}\\ 
        Alexander Schoch & \textbf{D}-CHAB  & \href{mailto:schochal@student.ethz.ch}{schochal@student.ethz.ch}
    \end{tabular}
}
\fancyhead[L]{M. Bregy, A. Schoch}
\fancyhead[R]{Parametric sensitivity}
\date{\dat}

\begin{document}
  \thispagestyle{plain}
  \maketitle

  \vspace{1cm}
	\begin{figure}[H]
		\centering
		\Large
    
		\begin{tabular}{rl}
			Assistant: & Simon Solari
		\end{tabular}
	\end{figure}
	\Large
	\begin{center}
		laboratory for chemical engineering at ETH Zürich
	\end{center}
	\normalsize
	\noindent\rule{\textwidth}{1pt}
  \vspace{-1cm}
  
  \paragraph{Abstract}{The aim of the experiment was to understanding the importance of the parametric sensitivity investigated on the exothermic hydrolysis of acetic anhydride catalysed by sulphuric acid. The reaction was run in a batch reactor with initial temperatures of the reactants of $T^\text{i} = \SI{61}{\celsius}$, \SI{63}{\celsius} and \SI{65}{\celsius} while cooling it with constantly flowing water with a temperature of $T = T^\text{i}$ with a volumetri flow of $Q = \SI{68}{\liter\per\hour}$. During the reaction, the temperature of the mixture was measured, resulting in a bell-shaped curve with a maximum $T^\text{max}$. At $T^\text{i} = \SI{65}{\celsius}$, this $T^\text{max}$ reached the boiling point of water (which is not quite \SI{100}{\celsius}), leading to a thermal runaway. From the measured $T^\text{max}$, a \texttt{Fortran} program was used to apply a model and fit it through the points while also calculating the sensitivity $S(T^\text{i})$, which reached a maximum at $T^\text{i} = \SI{62.5}{\celsius}$.}\newline
	\noindent\rule{\textwidth}{1pt}

	\vspace{\fill}
  \doubleSignature{Michael Bregy}{Alexander Schoch}{Zürich, }

	\newpage
	
  \section{Introduction}
  A chemical system is affected by many physicochemical parameters like temperature, pressure or concentrations. In our experiment, the chemical system is a well-stirred batch reactor and the focus is only on the temperature as system variable. The sensitivity of the system's behavior to changes in parameters is known as parametric sensitivity. If a system works in a parametrically sensitive region, its performance changes heavily with small variations in parameters. This is of high importance for the industry, which design and operate scaled-up chemical reactors that can leads to massive runaways or explosions.\cite{para}
  \par\bigskip
  In this experiment, the parametric sensitivity was investigated on the homogenous exothermic hydrolysis of acetic anhydride catalysed by sulphuric acid (eq. \ref{fig:rkt}) in a well-stirred batch reactor.

  \begin{equation}\label{fig:rkt}
    \schemestart
      \chemfig{-[:30](=[2]O)-[:-30]O-[:30](=[2]O)-[:-30]} \arrow{0}[,0]\+ \chemfig{H_2O} \arrow{->[\ce{H+}]} 2 \arrow{0}[,0] \chemfig{-[:30](=[2]O)-[:-30]OH}
    \schemestop
  \end{equation}
  
  \parmedskip
  A runaway may occur in a batch reactor in two main mechanisms. The first one occurs if the rate of heat generation is faster than the rate of heat removal by the cooling system. This leads to a continuous rise of temperature in the reactor and leads to thermal runaway. The second one involves chain branching processes, for example reactions produce two or more active species from a single one, which under certain conditions may accelarate the chemical reactions leading to a chain branching-induced explosion. This type occurs only in a complex reacting system. In the following, only the thermal runaway is considered, whereat many theories were described by Semenov. A runaway can be classfied in a geometry based type and sensitivity based type. The sensitivity based type is related to the parametric sensitivity while the geometry one is based on geometric feature of the profile of a system variable such temperature or heat-release rate.\cite{wu}
  \parmedskip
  If an $n$-th order reaction occurs in a closed vessel (like a batch reactor), the concentration and temperature are assumed to be uniform. The dynamics can be described by the mass (eq. \ref{mass_balance}) and energy balance (eq. \ref{energy_balance}).

  \begin{equation}
    \frac{\text{d}C}{\text{d}t} = - k(T)*C^n
  \label{mass_balance}
  \end{equation}
  
  \begin{equation}
    \rho* c_\text{v}*\frac{\text{d}T}{\text{d}t} = -\Delta H*k(T)*C^n - S_\text{v}*U*(T-T_\text{a})
  \label{energy_balance}
  \end{equation}
  
  with the inital conditions (IC's) $C = C^\text{i}$, $T = T^\text{i}$ at $t = 0$, where $C$ is the reactant concentration in \si{\mol\per\cubic\meter}, $t$ the time in \si{\second}, the reaction rate constant $k$ in $(\si{\mol\per\cubic\meter})^{1-n}\si{\per\second}$, $\rho$ the density of the solution in \si{\kilogram\per\cubic\meter}, $c_\text{v}$ the specific heat capactiy in \si{\joule\per\kelvin\per\mol}, $T$ the temperature in K, $T_\text{a}$ the ambient temperature in K, $\Delta H$ the heat of reaction \si{\joule\per\mol}, $S_\text{v}$ the external surface area per unit volume in \si{\square\meter\per\cubic\meter} and $U$ the overall heat transfer coefficient in \si{\joule\per\square\meter\per\second\per\kelvin}. 
  \par\medskip

  These equations can be rewritten in dimensionless form,
  
  \begin{equation}
  \frac{\dd x}{\dd\tau} = \exp\left(\frac{\theta}{1+\frac{\theta}{\gamma}}\right)*(1-x)^n = F_1(x,\theta)
  \label{dimless1}
  \end{equation}
  
    \begin{equation}
      \frac{\dd\theta}{\dd\tau} = B* \exp\left(\frac{\theta}{1+\frac{\theta}{\gamma}}\right)*(1-x)^n - \frac{B}{\psi}*(\theta - \theta_\text{a}) = F_2(x,\theta)
  \label{dimless2}
  \end{equation}
  and the IC's $x = 0$, $\theta = 0$ at $\tau = 0$, with the following dimensionless variables
  
  \begin{equation}
    x = \frac{C^\text{i} - C}{C^\text{i}},\thickspace \theta = \frac{T^\text{i} - T}{T^\text{i}}, \thickspace \tau = t*k(T^\text{i})*(C^\text{i})^{(n-1)},
  \label{dimlessvar}
  \end{equation}
  and the dimensionless parameters:
  
  \begin{equation}
    B = \frac{(-\Delta H) * C^\text{i}}{\rho * c_\text{v} * T^\text{i}} * \gamma ;\thickspace \gamma = \frac{E}{R_\text{g} *T^\text{i}} ;\thickspace \psi = \frac{(-\Delta H) * k(T^\text{i}) * (C^\text{i})^n}{S_\text{v} * U * T^\text{i}} * \gamma
  \label{B}
  \end{equation}
  
  where $x$ is the reactant conversion, $\tau$ the dimensionless time, $\theta$ the dimensionless temperature, $\gamma$ the dimensionless activation energy or Arrhenius parameter, $B$ the heat-of-reaction parameter, $\psi$ the Semenov number, which describes the ratio between heat-release potential of the reaction to the heat-removal potential through cooling, and $\theta_\text{a}$ the ambient temperature, $E$ the activation energy in \SI{}{\joule\per\mol} and $R_\text{g}$ the universal gas constant in \SI{}{\joule\per\kelvin\per\mol}. The superscript i refers always to the initial condition.\cite{wu}
  \parmedskip
  By using these definitions, it can be seen that the system can be fully described by the five parameters $n$, $\gamma$, $B$, $\psi$ and $\theta_\text{a}$. The first two characterize the kinetics of the reaction while the three other describe the physicochemical quantities. 
  \parmedskip
  The sensitivity criteria can often only be applied to reacting systems where the temperature profil exists, which is not always the case. In addition, these criteria do not give any information about the extent or intensity of the runaway. To overcome these limitations, new criteria were developed based on the concept of parametric sensitivity. One of this criteria is the Morbidelli and Varma (MV) criterion, which will be explained in the following.
  \parmedskip
   If the initial value of temperature is located on the left-hand side of the explosion limit curve, the reaction proceeds slowly. If the initial value increases, so it moves to the right-hand side of the curve, the reaction proceeds fast and leads to a temperature maximum, which is characterisitic for a system. For a fixed initial pressure, the critical condition for a runaway can be defined as the maximal value of $T^\text{i}$ at which the system does not undergo thermal explosion. This definiton implies that near the runaway boundary, the system became sensitive to small changes in initial temperature. This region of the system is called parametrically sensitive. If the initial temperature is far away from runaway explosion, the system is insensitive. The criteria should indentify this region in order to locate the boundaries between explosive and nonexplosive behavior.
  \parmedskip
  In order to find that out, the normalized sensitivity will be used. As a thermal runaway in a batch reactor is considered, the interest is focused on the sensitivity of the temperature maximum $\theta^\star$, such that the normalized sensitivity has the following form:
  
 \begin{equation}
 S(\theta^{\star}; \phi) = \frac{\phi}{\theta^{\star}}*\left(\frac{\partial\theta^{\star}}{\partial\phi}\right) = \frac{\phi}{\theta^{\star}}* s(\theta^{\star} ; \phi),
 \end{equation} 
  where $\phi$ is one of the model parameters. In the MV criterion, the parametrically sensitive region is defined where the absolute value of the normalized sensitivity of a temperature maximum reaches a maximum. To calculate this normalized sensitivity value, a program written in \texttt{Fortran}, developed by Dr. Hua Wu, can be used.\cite{wu} 
  

  	
\section{Experimental}
The first step was to prepare a \SI{0.025}{\molar} sulphuric acid solution. For that, \SI{2.54}{\gram} of sulphuric acid (Fischer Scientific, >95\%) was weighted in a flask and filled up with distilled water (DI water) to \SI{1036.4}{\gram}. \SI{180}{\gram} of this solution will be transferred to another flask. A second flask was filled up with \SI{170}{\gram} DI water. Then \SI{378}{\gram} of acetic anhydride (Sigma- Aldrich, >99\%) was weighted in a flask which was placed in a cryostat at a determined temperature. The exact amounts of the different chemicals and the different reaction temperature can be found in table \ref{weight}.
\parmedskip

	\begin{longtable}{rllll}
		\caption{Weight-in of sulphuric acid, distilled water and acetic anhydride at the different reaction temperature of each experiment.}
    \label{weight}\\
    \toprule
    \textbf{Experiment}  & \textbf{Temperature (\textdegree{}C)} & \textbf{\ce{H2SO4} (g)} & \textbf{DI water (g)} & \textbf{\ce{C4H6O3} (g)} \\ \midrule\endhead
		1           & 61 & 179.96 & 169.98 & 378.06             \\
		2       & 63 &180.05&170.06&378.09           \\
		3       & 65 &179.98&170.02&378.12             \\ \bottomrule
	\end{longtable}
  
 The flasks containing DI water and sulphuric acid were poured through a glass funnel into the reactor and preheated the solution to the desired temperature (see table \ref{weight}) under agitation (\SI{500}{rpm}). The cooler of the reactor has a cooling flow rate of \SI{68}{\liter\per\hour}, which was set with the S2000 controller, and the jacket of the reactor was evacuated using the vacuum pump. As the acetic anhydride had reached the desired temperature in the cryostat, it was poured into the reactor quickly. Make sure that the valve into and out of the reactor was closed. Immediately after this happened, the data acquisition was started in the computer program (LabView). The fume hood was closed and the temperature variation was observed. When the experiment was finished the data acquisition was stopped and the data were exported. After each experiment the reactor was emptied and poured to the waste container. Before the next experiment was started the reactor was washed with DI water. In the third experiment at \SI{65}{\celsius}, a runaway occurs, so that a safety valve was opened where the reaction solution flew through into a big flask.\cite{para}
  
  \section{Results}
  %TODO: evtl. intro/procedure anpassen im Speziellen der teil mit der Sensitivität 

  For each initial temperature $T^\text{i}$, the temperature $T$ in respect to time was measured. As the batch was cooled throughout the experiment, the cooling power predominates over the reaction enthalpy at some point, resulting in a peak in the temperature profile, as visible in fig. \ref{fig:temp}.\par\medskip

  If the temperature reaches the boiling point of water at some point (which is not qute \SI{100}{\celsius} in Zurich), the pressure increases rapidly, resulting in a runaway. This happened for $T^\text{i} = \SI{65}{\celsius}$ in our case (for safety reasons through an outlet valve) with $T^\text{max} = \SI{98.33}{\celsius}$. The $T^\text{max}$ values for the other measurements were $T^\text{max}(\SI{61}{\celsius}) = \SI{82.75}{\celsius}$ and $T^\text{max}(\SI{63}{\celsius}) = \SI{92.65}{\celsius}$.

  \begin{mdframed}
    \begin{figure}[H]
      \centering
      \begin{minipage}{0.33\linewidth}
        \caption{Time plotted against temperature of our measurements. It is visible that the temperature of the third measurement reached almost \SI{100}{\celsius}, meaning that the solution started to boil and a runaway happened.}
        \label{fig:temp}
      \end{minipage}
      \begin{minipage}{0.66\linewidth}
        \includegraphics[width=\linewidth]{../plots/temp.pdf}
      \end{minipage}
    \end{figure}
  \end{mdframed}

  From those three curves, the maximum value was taken and plotted in \ref{fig:sensitivity}. With those points, the \texttt{Fortran} program could more or less reasonably fitted a curve onto this data, giving a $T^\text{max}$ for each $T^\text{i}$. Additionally, the same program generates data for the sensitivity $S$, which has its maximum at $T^\text{i} = \SI{62.5}{\celsius}$, meaning that the temperature varies the most with a tiny nudge in initial temperature.
 
  \begin{mdframed}
    \begin{figure}[H]
      \centering
      \begin{minipage}{0.66\linewidth}
        \includegraphics[width=\linewidth]{../plots/sensitivity.pdf}
      \end{minipage}
      \begin{minipage}{0.33\linewidth}
        \caption{From the measured $T^\text{max}$, the \texttt{Fortran} program simulated $T^\text{max}$ for a range of $T^\text{i}$ and calculated the sensitivity $S$ from that simulation.}
        \label{fig:sensitivity} 
      \end{minipage}
    \end{figure}
  \end{mdframed}
   

   \section{Conclusions}

   The aim of this experiment was to get a feeleing for how a system can change over time with some input parameters. This is especially important in industry, as a runaway has to be avoided at all cost. Therefore, a chemical engineer would try (in this example) to run this reaction at maxiumum \SI{50}{\celsius}, even though the reaction rate might be a bit lower. If it is necessary, however, to run a reaction in a parametric sensitive regin, it is of great importance to meet as many precautions in order to prevent a runaway and diminish the damage if one should occur.

 

  \newpage
  \normalfont

   \bibliographystyle{unsrt}
   \bibliography{Literature_parametric}
  % TODO: insert every nocite (references without \cite{})
   %\nocite{citeKey}
   %\nocite{citeKey2}
   \newpage
   \section{Appendix}

   \subsection{Code}

   \includecode{../python/temp.py}{\texttt{python} code for figure \ref{fig:temp}}{py}
   \includecode{../python/sensitivity.py}{\texttt{python} code for figure \ref{fig:sensitivity}}{py}
   %TODO: Table with the constants/parameters from the excel file
   
   \includepdf[pages=-]{../include/journal.pdf}
\end{document}
