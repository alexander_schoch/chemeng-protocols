# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# 4 rows à 30 minutes (0-29) (measurement every minute), all four measurements are in the same row
DATA61 = "../data/data_61.csv"
data61 = np.genfromtxt(DATA61, delimiter=",").transpose()
DATA63 = "../data/data_63.csv"
data63 = np.genfromtxt(DATA63, delimiter=",").transpose()
DATA65 = "../data/data_65.csv"
data65 = np.genfromtxt(DATA65, delimiter=",").transpose()


plt.plot(data61[0] - data61[0][0],data61[1],linewidth=1,label=r'$T^\text{i} = \SI{61}{\celsius}$', linestyle='-', color='black')
plt.plot(data63[0] - data63[0][0],data63[1],linewidth=1,label=r'$T^\text{i} = \SI{63}{\celsius}$', linestyle='-.', color='black')
plt.plot(data65[0] - data65[0][0],data65[1],linewidth=1,label=r'$T^\text{i} = \SI{65}{\celsius}$', linestyle=':', color='black')


# Axis labels
#plt.ylabel(r'RFU', fontsize=16)
plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$T$ / \si{\celsius}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/temp.pdf')
