# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.interpolate import interp1d

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

DATA = "../data/simulation.csv"
data = np.genfromtxt(DATA, delimiter=",").transpose()

Tin = np.array([61, 63.3, 65])
Tmax = np.array([82.75, 92.65, 98.33])

plt.scatter(Tin, Tmax, s=10, c='black', label=r'measured $T^\text{max}$')

plt.plot(data[0] - 273.15, data[3] - 273.15, linewidth=1, color='black', label=r'simulated $T^\text{max}$')
plt.plot(data[0] - 273.15, data[6], linewidth=1, linestyle='-.', label=r'Sensitivity $S$', color='black')

print(data[0][np.where(data[6] == 191.1)] - 273.15)
# Axis labels
plt.xlabel(r'$T^\text{i}$ / \si{\celsius}', fontsize=16)
plt.ylabel(r'$T^\text{max}$ / \si{\celsius}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/sensitivity.pdf')
