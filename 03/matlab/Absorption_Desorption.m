A = load('ab_desorption_In2O3ZrO2.txt');
%absorption
A1 = A(:,1); % relative pressure ZrO2/In2O3
A2 = A(:,2); % Quantity absorbed ZrO2/In2O3
%desorption
A3 = A(:,3); % relative pressure ZrO2/In2O3
A4 = A(:,4); % Quantity absorbed ZrO2/In2O3
figure
plot(A1,A2);
hold on 
plot(A3,A4);
xlabel('Relative pressure (p/p^0)');
ylabel('Quantity Absorbed (cm^3/g STP)');
legend('Absoprtion','Desorption');

C = load('ab_desorption_PdZrO2.txt');
% absorption
C1 = C(:,1); % relative pressure Pd/ZrO2
C2 = C(:,2); % Quantity absorbed Pd/ZrO2
% desorption
C3 = C(:,3); % relative pressure Pd/ZrO2
C4 = C(:,4); % Quantity absorbed Pd/ZrO2
figure
plot(C1,C2);
hold on
plot(C3,C4);
xlabel('Relative pressure (p/p^0)');
ylabel('Quantity Absorbed (cm^3/g STP)');
legend('Absoprtion','Desorption');
% ZrO2 support 
B = load('ab_desorption_ZrO2_alfa.txt');
% absorption
B1 = B(:,1); % relative pressure Pd/ZrO2
B2 = B(:,2); % Quantity absorbed Pd/ZrO2
% desorption
B3 = B(:,3); % relative pressure Pd/ZrO2
B4 = B(:,4); % Quantity absorbed Pd/ZrO2
figure
plot(B1,B2);
hold on
plot(B3,B4);
xlabel('Relative pressure (p/p^0)');
ylabel('Quantity Absorbed (cm^3/g STP)');
legend('Absoprtion','Desorption');