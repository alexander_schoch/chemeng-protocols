# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
import pandas as ps

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

data = np.array(ps.read_csv('../data/Urs_ZrO2_bottle.csv')).transpose()


plt.plot(data[0], data[1], linewidth=1, color='black')
plt.text(24, 430, r'\SI{28}{\degree}', fontsize=16)
plt.text(30, 360, r'\SI{32}{\degree}', fontsize=16)

# Axis labels
plt.xlabel(r'$2\theta$ / \si{\degree}', fontsize=16)
plt.ylabel(r'Intensity / a.u.', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/Urs_ZrO2_bottle.pdf')
