# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
import pandas as ps

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

data = np.array(ps.read_csv('../data/Nano_30__2wt_Pd_ZrO2-CP-500-3h.csv')).transpose()

plt.plot(data[0], data[1], linewidth=1, color='black')

# Axis labels
plt.xlabel(r'$2\theta$ / \si{\degree}', fontsize=16)
plt.ylabel(r'Intensity', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/XRD_Pd-ZrO2.pdf')
