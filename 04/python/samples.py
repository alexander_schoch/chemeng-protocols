# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# 4 rows à 30 minutes (0-29) (measurement every minute), all four measurements are in the same row
DATA = "../data/yourData_python.txt"
BG = "../data/backgrounds.txt"
NUM_SAMPLE = 0
NUM_CURVE = 2
NUM_BG = 0
NUM_BG_CURVE = 2
NUM_POINTS = 38 # number of points for linear region
SLOPE_CAL = 1330
AMOUNT_AK1 = np.array([0.01, 0.05, 0.1]) # ng

data = np.genfromtxt(DATA, delimiter=",")
bg = np.genfromtxt(BG, delimiter=",")

bg_sample = np.mean(bg[0][NUM_BG_CURVE*38:NUM_BG_CURVE*38+38])
bg_reagent = np.mean(bg[1][NUM_BG_CURVE*38:NUM_BG_CURVE*38+38])

conc = (data[NUM_SAMPLE][NUM_CURVE*38:NUM_CURVE*38+38] - bg_sample - bg_reagent) / SLOPE_CAL


print(data[NUM_SAMPLE][NUM_CURVE*38 + 14] - bg_sample - bg_reagent)
print('======================================')
print('Sample Background:  ' + str(bg_sample))
print('Reagent Background: ' + str(bg_reagent))
print('======================================')

plt.scatter(np.arange(0,38),conc, s=4, c='black')

def lin(x, m, b):
    return m * x + b

parms, cov = curve_fit(lin, np.arange(0, NUM_POINTS), conc)

xarr = np.linspace(0, NUM_POINTS, 100)
plt.plot(xarr, lin(xarr, *parms), color='red', linewidth=1)

print('regression slope:     ' + str(parms[0]))
print('regression intercept: ' + str(parms[1]))
print('======================================')
print('Activity: ' + str(parms[0] / AMOUNT_AK1[0]))
print('======================================')

# Axis labels
plt.xlabel(r'$t$ / min', fontsize=16)
#plt.ylabel(r'RFU', fontsize=16)
plt.ylabel(r'$n_\text{ATP}$ / nmol', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/samples.pdf')
