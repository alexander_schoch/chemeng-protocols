# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# 4 rows à 30 minutes (0-29) (measurement every minute), all four measurements are in the same row
DATA = "../data/calibrationData_python.txt"
NUM_CAL = 3
NUM_CURVE = 2
NUM_POINTS = 10


data = np.genfromtxt(DATA, delimiter=",")
mean_abs = np.mean(data[NUM_CAL][NUM_CURVE*30 + (30 - NUM_POINTS):NUM_CURVE*30+30])

values = np.array([0,0,0,0])

for i in np.arange(0,4):
    values[i] = np.mean(data[i][NUM_CURVE*30 + (30 - NUM_POINTS):NUM_CURVE*30+30])

absorption = values - values[0]
conc = np.array([0, 0.2, 0.4, 0.6]) # [nmol]

#plt.xlabel(r'$t$ / min', fontsize=16)
#plt.ylabel(r'absorption', fontsize=16)
#plt.scatter(np.arange(0,30),data[NUM_CAL][NUM_CURVE*30:NUM_CURVE*30+30], s=4)
#plt.plot([0, 30], [mean_abs, mean_abs], color='black')
#plt.tight_layout()
#plt.savefig("../plots/cal_data.pdf")


#conc = [1,2,3,4]
#absorption = [1,2,3,4]

A = 2.740
b = 1 # [cm]
eps = 0.405 # [L g-1 cm-1]

c = A * 1e3 / (b * eps) # [ng muL-1]
AK1 = np.array([10,50,100])


V1 = 4 * 2000 / c
print(V1)


def linear(x, m, b):
    return m * x + b


parms, cov = curve_fit(linear, conc, absorption, p0=[1, 1])
stder = np.sqrt(np.diag(cov))

xarr = np.linspace(conc[0], conc[-1],100)

ci = stder * 2.776 / np.sqrt(4)

#upper = linear(xarr, *parms + ci)
#lower = linear(xarr, *parms - ci)
upper = linear(xarr, parms[0] + ci[0], 0)
lower = linear(xarr, parms[0] - ci[0], 0)

plt.fill_between(xarr, upper, lower, color='#aaaaaaaa')

plt.scatter(conc, absorption, s=4, c='black')
plt.plot(xarr, linear(xarr, parms[0], 0), linewidth=1, color='black')

print('=================================')
print('A(c) = ' + str(parms[0]) + '(' + str(ci[0]) + ') * x + ' + str(parms[1]) + '(' + str(ci[1]) + ')')
print('=================================')

# Axis labels
plt.xlabel(r'$n_\text{ATP}$ / nmol', fontsize=16)
plt.ylabel(r'OD \SI{570}{\nano\meter}', fontsize=16)
#
## Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
#
## make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/cal.pdf')
