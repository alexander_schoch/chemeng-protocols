# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy import integrate

# Latex implementation: Process all text with latex
#plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

DATA = "../data/ChromatogramSEC_python.asc"

data = np.genfromtxt(DATA, delimiter=',').transpose()

time = data[0] # [min]
absorption = data[1] # [Absorption units]
conductivity = data[3]
   
# integral

BORDER1 = 400
BORDER2 = 484

integ1 = integrate.simps(absorption[480:-1], time[480:-1])
integ2 = integrate.simps(absorption[410:480], time[410:480])
integ3 = integrate.simps(absorption[:410], time[:410])

print(integ1, integ2, integ3)

# plot

plt.plot(time[300:650], absorption[300:650], linewidth=1, color='black')
plt.fill_between(time[BORDER2:650], absorption[BORDER2: 650], np.linspace(0,0,650-BORDER2), color="#07575baa", label=r'Protein')
plt.fill_between(time[BORDER1:BORDER2], absorption[BORDER1: BORDER2], np.linspace(0,0,BORDER2-BORDER1), color="#66a5adaa")
plt.fill_between(time[300:BORDER1], absorption[300: BORDER1], np.linspace(0,0,BORDER1-300), color="#c4dfe6aa")

# Axis labels
plt.xlabel(r'$t$ / min', fontsize=16)
plt.ylabel(r'absorbance (\SI{280}{\nano\meter}) / \si{mAu}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/chromatogram.pdf')
