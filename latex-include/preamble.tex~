\documentclass[12pt, a4paper]{article}

%\usepackage{mathpazo}
%\usepackage{euler}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry} %old: lr1.7cm, tb2.5cm
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage[light]{CormorantGaramond}
%\usepackage{multimedia}
\usepackage{xcolor}
\usepackage{xpiano}
%\usepackage{subfig}
\usepackage{blindtext}
\usepackage{url}
\usepackage{array}
\usepackage{listings}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usepackage{eso-pic}
\usepackage{lastpage}
\usepackage{xltxtra}
\usepackage{textcomp}
\usepackage{titlesec}
%\usepackage{pgfornament}
\usepackage{titletoc}
\usepackage{chemfig}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage[font={it}]{caption}
\usepackage{setspace}
%\usepackage{chemmacros}
\usepackage{lipsum}
\usepackage{cancel}
\usepackage{subfigure}
\usepackage{booktabs}
\usepackage{longtable}


\titleformat{\section}
[block]
{\normalfont\Large\bfseries}
{\hskip 2pt\oldstylenums{\myTheSection}\hskip 2pt}
{10pt}
{\Large\bfseries\scshape}

\titleformat{\subsection}
[block]
{\normalfont\Large\bfseries}
{\hskip 2pt\oldstylenums{\myTheSubsection}\hskip 2pt}
{10pt}
{\Large\bfseries\itshape}


\titlecontents{section}
[15mm]
{\normalfont\bfseries\medskip}
{\contentslabel{9mm}\scshape}
{\hspace*{-9mm}}
{\titlerule*[1pc]{.}\oldstylenums{\contentspage}}

\titlecontents{subsection}
[24mm]
{\normalfont\bfseries}
{\contentslabel{12mm}\itshape}
{\hspace*{-12mm}}
{\titlerule*[1pc]{.}\oldstylenums{\contentspage}}

\titlecontents{subsubsection}
[35mm]
{\normalfont\bfseries}
{\contentslabel{18mm}\footnotesize}
{\hspace*{-12mm}}
{\titlerule*[1pc]{.}\oldstylenums{\contentspage}}


%titleformat{\section}[frame]{\normalfont}{\filright\footnotesize\enspace SECTION \thesection\enspace}{8pt}{\Large\bfseries\filcenter}


\newfontfamily{\computerFont}{CormorantGaramond-Light}
%\setmonofont{Courier}
\setmonofont{CormorantGaramond-Light}

\renewcommand{\textbf}[1]{{\normalfont\bfseries\oldstylenums{#1}}}
\renewcommand{\textit}[1]{{\normalfont\itshape\oldstylenums{#1}}}
\renewcommand{\textsc}[1]{{\normalfont\scshape\oldstylenums{#1}}}
\renewcommand{\thefigure}{\oldstylenums{\arabic{figure}}}
\renewcommand{\thetable}{\oldstylenums{\arabic{table}}}
\renewcommand{\theequation}{\computerFont\thesection |\ \oldstylenums{\arabic{equation}}}

\newcommand{\titel}[1]{\title{\normalfont\textsc{#1}}\fancyhead[L]{\normalfont\textsc{#1}}}
\newcommand{\autor}[1]{\author{#1}\fancyhead[R]{\textsc{#1}}}
\newcommand{\fett}[1]{\normalfont\textbf{\oldstylenums{#1}}}
\newcommand{\quotes}[1]{{\normalfont <<}#1{\normalfont >>}}
\newcommand{\parsmallskip}{\par\smallskip}
\newcommand{\parmedskip}{\par\medskip}
\newcommand{\absatz}{\par\bigskip}
\newcommand{\sct}[1]{\section{\underline{\textsc{#1}}}}
\newcommand{\subsct}[1]{\subsection{\itshape #1}}
\newcommand{\pfeil}{$\Rightarrow$\ }
\newcommand{\hoch}[1]{\textsuperscript{#1}}
\newcommand{\tief}[1]{\textsubscript{#1}}
%\newcommand{\num}[1]{\oldstylenums{#1}}
\newcommand{\degree}{$^\circ$}

\newcommand{\textfeld}[3][black]{\begin{center}
		\begin{tikzpicture}[whatever/.style={draw=#3!100, fill=#3!20, rounded corners=2ex, text width=\linewidth, very thick}]
		\node[whatever]at (20,20){\textcolor{#1}{#2}};
		\end{tikzpicture} \end{center}}

\newcommand{\textfeldCenter}[3][black]{\begin{center}
		\begin{tikzpicture}[whatever/.style={draw=#3!100, fill=#3!20, rounded corners=2ex, text width=\linewidth, very thick}]
		\node[whatever]at (20,20){\begin{center}\textcolor{#1}{#2}\end{center}};
		\end{tikzpicture} \end{center}}

\newcommand{\doubleSignature}[3][Alexander Schoch]{%[name1]{name2}{Ort}
	\parbox{\textwidth}{
		\centering #3 \today\\
		\vspace{2cm}

		\parbox{7cm}{
			\centering
			\rule{6cm}{1pt}\\
			#1
		}
		\hfill
		\parbox{7cm}{
			\centering
			\rule{6cm}{1pt}\\
			#2
		}
	}
}

\newcommand{\signature}[2][Alexander Schoch]{%[name]{ort}
	\parbox{\textwidth}{
		\centering #2 \computerFont\today\\
		\vspace{2cm}

		\parbox{7cm}{
			\centering
			\rule{6cm}{1pt}\\
			#1
		}
	}
}


\fancypagestyle{plain}{%
	\renewcommand{\headrulewidth}{0pt}%
	\fancyhf{}%
	\fancyfoot[C]{}%
}
\pagestyle{fancy}
\fancyfoot[C]{\textit{page \thepage\ of \pageref*{LastPage}}}

\date{\oldstylenums{last edited on \today}}
\titel{\textsc{Titel bitte ändern}}
\autor{Inhalt bitte mit \quotes{autor} ändern}


\DeclareMathSymbol{*}{\mathbin}{symbols}{"01}
\setcrambond{3pt}{1pt}{2pt}



\let\myTheSection\thesection
	\renewcommand{\thesection}{\oldstylenums{\myTheSection}}

\let\myTheSubsection\thesubsection
	\renewcommand{\thesubsection}{\oldstylenums{\myTheSubsection}}

\let\myTheSubsubsection\thesubsubsection
	\renewcommand{\thesubsubsection}{\oldstylenums{\myTheSubsubsection}}

\let\myTheParagraph\theparagraph
	\renewcommand{\paragraph}[1]{{\normalfont\bfseries\scshape\oldstylenums{#1}\hskip 15pt}}


\parindent0mm

%\let\myThePage\thepage
%	\renewcommand{\thepage}{ \normalfont\oldstylenums{\myThePage} }
