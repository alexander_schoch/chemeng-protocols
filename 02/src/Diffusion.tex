% This file is a template for the protocols


\input{../../latex-include/preamble.tex}
\input{../../latex-include/lstsetup.tex}
\input{../../latex-include/chemicalMacros.tex}



% TODO: better title
\newcommand{\dat}{17\textsuperscript{th} of October, 2019}
\newcommand{\tit}{Determination of Diffusion Coefficient of CO\textsubscript{2} in Air and \newline Light Triggered Dissolution of Gold }


\title{\tit}
\author{
    \begin{tabular}{rll}
        Michael Bregy & \textbf{D}-CHAB  & \href{mailto:bregym@student.ethz.ch}{bregym@student.ethz.ch}\\ 
        Alexander Schoch & \textbf{D}-CHAB  & \href{mailto:schochal@student.ethz.ch}{schochal@student.ethz.ch}
    \end{tabular}
}
\fancyhead[L]{M. Bregy, A. Schoch}
%\fancyhead[R]{\tit}
\date{\dat}

\begin{document}
  \thispagestyle{plain}
  \maketitle  \vspace{0.8cm}
	\begin{figure}[H]
		\centering
		\Large
    
		\begin{tabular}{rl}
      Assistant: & Kohll Xavier \\ & Antkowiak Philipp \\ Group: & 9
		\end{tabular}
	\end{figure}
	\Large
	\begin{center}
		Laboratory for Chemical Engineering at ETH Zürich
	\end{center}
	\normalsize
	\noindent\rule{\textwidth}{1pt}
  \vspace{-1cm}

	\paragraph{Abstract} 
  The aim of the first experiment was to determine the diffusion coefficient $D$ of CO\textsubscript{2} in air. The concentrations of CO\textsubscript{2} were measured during $\SI{30}{\min}$ in a tube with \ce{CO2} sensors at five different positions. The diffusion coefficients $D$ were calculated for different times. The calculated diffusion coefficients were $\SI{0.5 \pm 0.3}{\square\cm\per\second}$ for $\SI{10}{\min}$ , $\SI{0.20 \pm 0.11}{\square\cm\per\second}$ for $\SI{20}{\min}$ and $\SI{0.11 \pm 0.07}{\square\cm\per\second}$ for $\SI{30}{\min}$. Comparing the diffusion coefficient at $\SI{30}{\min}$ with the literature value $D = \SI{0.148}{\square\cm\per\second}$  shows that the measured diffusion coefficient correlates well. Moreover, the experiment showed that a gas diffuses less than $\SI{1}{\meter}$ in $\SI{30}{\min}$. In the second experiment, it was visible that the gold dissolution worked best with white and blue light, whereas red and green showed quite poor results.\newline
	\noindent\rule{\textwidth}{1pt}
	
	%Signature
	\vspace{\fill}
  \doubleSignature{Michael Bregy}{Alexander Schoch}{Zürich, }

	\newpage

	
\section{Introduction}

\subsection{Determination of Diffusion Coefficient of CO\textsubscript{2}} 
Diffusion is defined as transferred mass by a random motion of molecules across a concentration gradient. From practical experience it can be said that the diffusion occurs from the region with higher concentration to the region with lower concentration. After an infinite time the concentrations reached an equilibrium so that the concentration is staying constant everywhere. To reach the equilibrium it depends also on the matter of the medium where diffusion occurs. In gases the diffusion coefficient amount to approximately $\SI{1}{\square\cm\per\second}$, in liquids $\SI{10^{-5}}{\square\cm\per\second}$ and in solids $\SI{10^{-10}}{\square\cm\per\second}$.\cite{icb}
\par\medskip
Diffusion without chemical reaction and in unsteady state, means that concentration is dependent on position and in time, can be described with  Fick's second law as followed (eq. \ref{eq:1}).

\begin{equation}
	\frac{\partial{c_A}}{\partial{t}} = D\cdot\frac{\partial^2{c_A}}{\partial^2{x}},
	\label{eq:1}
\end{equation} 

with $c_A$ the concentration of the substance $A$, $D$ the diffusion coefficient and $x$ the direction where the diffusion occurs. To solve this differential equation a number of inital and boundary condititons are needed. Under assumption that diffusion occurs only in $x$ direction and the stationary substance occupies an infinite region, following conditions are received:

\begin{equation}
  t=0 \text{ for } x<0 \rightarrow c_A=c_{A0} \,\, \text{ and for } x>0 \rightarrow c_A=0
	\label{eq:2}
\end{equation} 

Now, the differential equation (eq. \ref{eq:1}) can be solved for the concentration of $A$.

\begin{equation}
  c_A(x,t) = \frac{1}{2}\cdot c_{A0} \cdot \text{erfc} \left(\frac{x}{2\sqrt{D\cdot t}}\right)
	\label{eq:3}
\end{equation} 
Where $\text{erfc}(x)$ is the complementary error function. With  the time when substance $A$ diffuses, the equation can be described more commonly like in eq. \ref{eq:4}.

\begin{equation}
  c_A(x,t) = c_{A0m} + \frac{1}{2}(c_{A0M} - c_{A0m}) \cdot \text{erfc} \left(\frac{x}{2\sqrt{D\cdot t}}\right)
\label{eq:4}
\end{equation} 

Where $c_{A0M}$ is the concentration of $A$ in the region with higher concentration and $c_{A0m}$ is the region with the lower concentration. Basing on eq. \ref{eq:4}, a modified variant of this was used for the experimental setup.  

\begin{equation}
  c_A(x,t) = c_{A0m} + \frac{1}{2}(c_{A0M} - c_{A0m}) \cdot \text{erfc} \left(\frac{(x-50)}{2\sqrt{D\cdot t}}\right)
	\label{eq:5}
\end{equation} 

Out of eq. \ref{eq:5} the diffusion coefficient of $CO_2$ can be determined experimentally.\cite{icb}

\subsection{Light Triggered Dissolution of Gold}

Gold is a very valuable precious metal and thus resistant to a lot of chemicals. However, there are some reactions which can take place with gold, such as the dissolution in aqua regia. In this experiment, however, the relevant dissolution reaction of gold uses cyanide ions to form a water-soluble complex. The dissolution reaction, also known as \quotes{Elsner Equation}, can be formulated as follows:

\begin{equation}
  \label{eq:elsner}
  \schemestart
    4 \ce{Au} + 8 \ce{CN-} + \ce{O2} + 2 \ce{H2O} \arrow{->} 4 \ce{[Au(CN)2]-} + 4 \ce{OH-}
  \schemestop
\end{equation}

It has to be noted, however, that cyanide ions are very toxic to humans. Therefore, it is necessary to generate those ions \quotes{in situ} in as low concentrations as possible. \par\medskip

To achieve this, it is possile to use the behaviour of potassium ferricyanide (\ce{K2[Fe(CN)6]}) to release cyanide ions upon excitation with light via

\begin{equation}
  \label{eq:cyandegeneration}
  \schemestart
    \ce{[Fe(CN)6]^{4-}} + 4 \ce{H2O} \arrow{<=>[\textit{h}\nu]} \ce{[Fe(CN)5 * H2O]^{3-}} + \ce{OH-} + \ce{HCN}
  \schemestop
\end{equation}

whereat prussic acid can undergo an acid-base reaction to form cyanide. This is also why a pH buffer should be used for the experiment.\par\medskip

With those two reactions, it is now an easy task to take a gold coated slide, cover it with pH buffer and aqueous solution of potassium ferricyanide and project some kind of image onto the slide in order to etch this image onto the slide.\par\medskip

In order to get the quality of the etching, an image called \quotes{1951 USAF resolution test chart} can be used. In order to get the resoluton of the image, ones takes the the smallest distinctive form and plug that into \cite{usaf}. 



\section{Procedure}
\subsection{Determination of Diffusion Coefficient of CO\textsubscript{2}}
The experimental setup (for a sketch see the lab journal) consist of a gas chamber with inlet and outlet ports, a membrane holder and a sealed PVC tube with five CO\textsubscript{2} sensores mounted in it. The PVC tube has a total length of \SI{100}{\centi\meter} and the sensors were positioned at equidistant points at $\SI{0}{\cm}$, $\SI{25}{\cm}$, $\SI{50}{\cm}$, $\SI{75}{\cm}$ and at $\SI{100}{\cm}$ The sensors were electrochemical sensors (MG-811) and were connected to a Arduino apparatus, which can calculate the concentrations in parts per million (ppm). The Arduino was connected to a computer. Next, the membrane holder and the gas chamber were locked together to the tube. Then, the gas bottle which containing CO\textsubscript{2}, was opened so that the pressure was around $\SI{0.2}{\bar}$. At the time when the gas bottle tube was connected to the gas chamber, the measurement was started to register the concentrations of CO\textsubscript{2}. After the experiment was running for $\SI{30}{\min}$, the data were exported to Excel. Due to the known total running time and measuring steps the time intervals can be determined. The diffusion coefficient of CO\textsubscript{2} were calculated at $\SI{10}{\minute}$, $\SI{20}{\minute}$ and at $\SI{30}{\minute}$. To minimize the residual sum of squares between the theoretical (eq. \ref{eq:5}) and experimental values and to find the best-fitting diffusion coefficient $D$, a \texttt{python} script (see sec. \ref{python}) was used.


\subsection{Light Triggered Dissolution of Gold}

\subsubsection{Experimental Setup}

The front lens of a standard beamer was removed and placed a bit further away from the light source in order to be able to project a small image. This beamer was then vertically mounted onto a cardboard box. At the focus point of the beamer, a platform was installed for the sample to put on. Because the used reaction is light sensitive, The cardboard box was covered with a courtain. \par\medskip

The beamer was connected to a laptop running \textsc{Microsoft PowerPoint} and an image manipulation program.

\subsection{Procedure}

First, a gold plated slide was mounted into the petri dish (which had a sticky ground). Afterwards, \SI{2}{\milli\liter} of pH 13 buffer and \SI{6}{\milli\liter} of \SI{0.5}{\molar} \ce{K4[Fe(CN)6]} solution were added. Meanwhile, the \textsc{1951 USAF resolution test chart} \cite{usaf} was colored in white (\texttt{\#ffffff}), exported as an image, imported with \textsc{PowerPoint} and projected in presentation mode onto the petri dish. After waiting for \SI{30}{\minute} each time, this process was repeated for a red (\texttt{\#ff0000}), green (\texttt{\#00ff00}) and blue (\texttt{\#0000ff}) test image.  \par\medskip




\section{Results \& Discussion}
\subsection{Determination of Diffusion Coefficient of CO\textsubscript{2}}
In fig. \ref{fig:conc2}, the measured concentrations of CO\textsubscript{2} at every of the five sensors was plotted against the total running time of $\SI{30}{\minute}$. What stands out is the fact, that at $t = \SI{0}{\minute}$ the initial concentrations are not equal at all sensors. The initial concentrations varies between $\SI{200}{ppm}$ and $\SI{350}{ppm}$. But it can be seen that the concentration is higher at the sensors closer to the membrane, which makes sense, as the diffusion takes some time to spread out. 
\par\medskip
The concentrations at sensors 1 and 2 are staying more or less constant after $\SI{15}{\minute}$, but a change of concentrations can be detected already after $\SI{1-2}{\minute}$ . The concentrations at sensors 3, 4 and 5 increased very slightly after $\SI{7}{\minute}$. As result of this observation, it can be said that the CO\textsubscript{2} gas diffuses approximately $\SI{1}{\meter}$ in $\SI{30}{\minute}$.

    \begin{mdframed}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\linewidth]{../plots/Conc2.pdf}
		\caption{Concentration of CO\textsubscript{2} depending on time at the five sensors. Sensor 1 corresponds to position $x = \SI{0}{\cm}$, sensor 2 to $x = \SI{25}{\cm}$, sensor 3 to $x = \SI{50}{\cm}$, sensor 4 to $x = \SI{75}{\cm}$ and sensor 5 to $x = \SI{100}{\cm}$}
		\label{fig:conc2}
	\end{figure}
\end{mdframed}
\par\bigskip
Then, the concentrations at the five sensors were compared at different times: In fig. \ref{fig:10min} at $\SI{10}{\minute}$, in fig. \ref{fig:20min} at $\SI{20}{\minute}$ and in fig. \ref{fig:30min} at $\SI{30}{\minute}$ and fig. \ref{fig:CO2} summarized all three. The measured concentrations are plotted as points, the theoretical and optimized concentration profile (eq. \ref{eq:5}) as a line with 95\% confidence interval. In all three figures, it can be observed that the measured concentrations are too low compared to the model, which is probably because the pressure of \ce{CO2} was not correct. Another explanation could be that the membrane was fixed insufficiently. After fitting the model to the measured data using \texttt{python}, following diffussion coefficients for CO\textsubscript{2} were received: $\SI{0.508 \pm 0.337}{\square\cm\per\second}$ for $\SI{10}{\min}$ , $\SI{0.202 \pm 0.111}{\square\cm\per\second}$ for $\SI{20}{\min}$ and $\SI{0.109 \pm 0.067}{\square\cm\per\second}$ for $\SI{30}{\min}$.  Comparing the diffusion coefficient at $\SI{30}{\min}$ with the literature value $D = \SI{0.148}{\square\cm\per\second}$ \cite{dd} shows that the determined diffusion coefficient is in a similar range.


\begin{mdframed}
  \begin{figure}[H]
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth]{../plots/10min.pdf}
      \caption{Measured (points) and theoretical (line) concentrations at the five sensor positions at a time of \SI{10}{\minute}. Using the fitted value for $D$, the model was applied and theoretical concentrations (line) were calculated with a \SI{95}{\percent} confidence interval (colored area).}
      \label{fig:10min}
    \end{subfigure}
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth]{../plots/20min.pdf}
      \caption{Measured (points) and theoretical (line) concentrations at the five sensor positions at a time of \SI{20}{\minute}. Using the fitted value for $D$, the model was applied and theoretical concentrations (line) were calculated with a \SI{95}{\percent} confidence interval (colored area).}
      \label{fig:20min}
    \end{subfigure}
	\end{figure}
\end{mdframed}

\begin{mdframed}
	\begin{figure}[H]
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth]{../plots/30min.pdf}
      \caption{Measured (points) and theoretical (line) concentrations at the five sensor positions at a time of \SI{30}{\minute}. Using the fitted value for $D$, the model was applied and theoretical concentrations (line) were calculated with a \SI{95}{\percent} confidence interval (colored area).}
      \label{fig:30min}
    \end{subfigure}
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth]{../plots/CO2.pdf}
      \caption{The measured and theoretical concentration profiles at the five sensor positions at a time of \SI{10}{\minute}, \SI{20}{\minute} and \SI{30}{\minute}}
      \label{fig:CO2}
    \end{subfigure}
	\end{figure}
\end{mdframed}

Even though the model does not really fit the measured data, it is still visible that the concentration of \ce{CO2} increases over time on all sensors.




\subsection{Light Triggered Dissolution of Gold}
 %TODO: länge gemessen test chart quadrat 2.15 mm (gemessen beim grünen, angenommen für alle) auf folie/internet 10mm -> korrekturfaktor miteinbeziehen. Mit Absorptionsspektrum erklären warum man das blaue am besten erkennen sollte.

  In order to calculate the resolution of the etched image, the rectangle on the \textsc{1951 USAF resolution test chart} image was measured, resulting in \SI{2.15}{\milli\meter} for the green projection. As the same image scale was used for all projections, this size was assumed to be constant throughout all four colors. This size was used to scale up the resolution to the intended \SI{10}{\milli\meter} rectangle size. \par\medskip

  Using \cite{usaf}, the following resolutions were gotten:

  \begin{mdframed}
    \begin{figure}[H]
      \caption{Table of the resolution for each color}
      \begin{longtable}{|r|l|l|}
        \toprule
        \textbf{Color} & \textbf{coordinate} & \textbf{Resolution} \\ \midrule
        white & $(-2, 6)$ & 2.0698 \\ 
        blue & $(-2, 6)$ & 2.0698 \\
        green & $(-1, 5)$ & 3.6930 \\ 
        red & - & - \\ \bottomrule
      \end{longtable}
    \end{figure}
  \end{mdframed}




    \begin{mdframed}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.5\linewidth]{../plots/gold_green.pdf}
		\caption{If a photo of the plate was taken by the smartphone, the resolution was insufficient and cannot be determined by the resolution test chart.} 
		\label{fig:gold_green}
	\end{figure}
\end{mdframed}
\par\bigskip

It is visible that while the picture was more visible with white and blue light, the reolution with green light is quite a bit higher. This could occur because less gold reacted in the green case, resulting in less blurring. This is quite analogous with the exposure time in photograpy.




  % TODO: bibliography (compile with biblatex template.tex)
  \newpage
  \normalfont

  \bibliographystyle{unsrt}
  \bibliography{Literature_diffusion}
  %TODO: Grossschreibung bei References 
  
 \newpage
  \section{Appendix}
  \subsection{Code}\label{code}
  \subsubsection{Python}\label{python}
  \includecode{../py/CO2.py}{Code for the calculation of the $D$ values and generation of fig. \ref{fig:CO2} using \texttt{python}}{py}
  \includecode{../py/CO2_10min.py}{Code for fig. \ref{fig:10min} using \texttt{python}}{py}
  \includecode{../py/CO2_20min.py}{Code for fig. \ref{fig:20min} using \texttt{python}}{py}
  \includecode{../py/CO2_30min.py}{Code for fig. \ref{fig:30min} using \texttt{python}}{py}
  \subsubsection{Matlab}\label{matlab}
  \includecode{../matlab/conc_time.m}{Code for fig. \ref{fig:conc2} using \texttt{matlab}}{py}
  %TODO: lab journal scan hinzufügen
  
  \includepdf[pages=-]{../include/journal_diff_gold.pdf}
\end{document}
