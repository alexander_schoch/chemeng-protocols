# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

print("==================================")
# copper block

cp = 0.38493 # [J g-1 K-1], taken from https://pubchem.ncbi.nlm.nih.gov/compound/23978 
m = 347.48 # [g]
dt = [16, 16] # s
dT = [1.8020, 1.7680] # K

dt = np.mean(dt)
dT = np.mean(dT)

Q = m * cp * dT / dt # [W]
Qin = 110 # [W]



# intel, superslient, thermaltake

A = 0.001023# [m2]
dT_env = np.array([19.670, 23.910, 55.300]) # [K]
dT_out = np.array([23.050, 28.320, 56.950]) # [K]

h = Q / (A * dT_env) # [W m-2 K-1]

print("Format: Intel, Supersilend, Thermaltake")
print("Heat Transfer Coefficient [W m-2 K-1]: ")
print(h)

# Water flow

F200 = 0.0185 # [L s-1]
F500 = 0.0455 # [L s-1]

rho = 1 # [g mL-1]

# Water cooling

cp = 4.2 # [J g-1 K-1]
dT_water = np.array([0.380, 0.01]) # [K]
dT_env_water = np.array([6.205, 5.255]) # [K]


Q_water = F200 * rho * dT_water * cp
h = Q / (A * dT_env_water) # [W m-2 K-1]

print()
print("Heat transfer coefficient [W m-2 K-1]:")
print(h)


print("==================================")

print(Q / 110) # 110 W
# Axis labels
#plt.xlabel(r'<++>', fontsize=16)
#plt.ylabel(r'<++>', fontsize=16)
#
## Grid
#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
#
## make margins nice and print the plot to a pdf file
#plt.tight_layout()
#plt.savefig('<++>')
