# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
import scipy.special as spc

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

t = 10

data = [[579, 482, 308, 230, 203],
        [621, 554, 340, 256, 217],
        [615, 568, 355, 278, 248]]

def opt(x, D, t, CM, Cm):
    return Cm + 0.5 * (CM - Cm) * spc.erfc((x-50) / (2 * np.sqrt(D * t)))

x = np.linspace(0,100,5)
y = np.array([1200, 900, 800, 750, 700])

plt.scatter(x,data[0],s=3,c='blue')

xarr = np.linspace(0,100,1000)

parms, cov = curve_fit(lambda x, D: opt(x, D, 600, data[0][0], data[0][4]), x, data[0])
plt.plot(xarr, opt(xarr, *parms, 600, data[0][0], data[0][4]), linewidth=1, color='blue', label=r"$t = \SI{10}{\min}$")
print("D(t = 10min) = ", *parms, " +- ", np.sqrt(*cov))

ci95 = np.sqrt(cov[0][0]) * 2.1318 / np.sqrt(5) 

plt.fill_between(xarr, opt(xarr, *parms + ci95, 600, data[0][0], data[0][4]), opt(xarr, *parms - ci95, 600, data[0][0], data[0][4]), color="#0101E888")

#plt.plot(xarr, opt(xarr, Davg, 600, data[0][0], data[0][4]), linewidth=1, color='blue', label=r"$t = \SI{10}{\min}$")
#plt.plot(xarr, opt(xarr, Davg, 1200, data[1][0], data[1][4]), linewidth=1, color='red', label=r"$t = \SI{20}{\min}$")
#plt.plot(xarr, opt(xarr, Davg, 1800, data[2][0], data[2][4]), linewidth=1, color='green', label=r"$t = \SI{30}{\min}$")




# Axis labels
plt.xlabel(r'$x$ / cm', fontsize=16)
plt.ylabel(r'$c$ / ppm', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('../plots/10min.pdf')


