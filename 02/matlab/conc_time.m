load("data.txt");

t = data(:,1);      % time intervals                    
x_0 = data(:,2);    % concentrations at 0 cm                     
x_25 = data(:,3);   % concentrations at 25 cm                    
x_50 = data(:,4);   % concentrations at 50 cm                       
x_75 = data(:,5);   % concentrations at 75 cm                       
x_100 = data(:,6);  % concentrations at 100 cm   

plot(t,x_0,t,x_25,t,x_50,t,x_75,t,x_100);
xlabel("time [s]");
ylabel("CO_2 [ppm]");
legend('Sensor 1','Sensor 2','Sensor 3','Sensor 4','Sensor 5','location','eastoutside')


